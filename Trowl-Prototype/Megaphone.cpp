#pragma once
#include "Megaphone.h"
#include <iostream>
#include "SFML\Graphics.hpp"
#include "AnimatedSprite.h"

Megaphone::Megaphone()
{
	std::cout << "Megaphone Created Using Wrong Constructor" << std::endl;
}				 

//Megaphone::~Megaphone() {}

Megaphone::Megaphone(float xPos, float yPos){
	sprite = new AnimatedSprite(
		"../assets/AnimatedObjects/PowerUpMegaphoneSpriteSheet.png",
		"../assets/AnimatedObjects/PowerUpMegaphoneSpriteSheet.txt");
	sprite->setAnimationPosition(xPos, yPos);

	rect = new sf::FloatRect(
		xPos,
		yPos,
		sprite->getSprite()->getLocalBounds().width,
		sprite->getSprite()->getLocalBounds().height);
	}

void Megaphone::Update(float deltaTime)
{
	sprite->update(deltaTime);
}

sf::FloatRect * Megaphone::getRect()
{
	return rect;
}

sf::Sprite * Megaphone::getSprite()
{
	return sprite->getSprite();
}

int Megaphone::getType()
{
	return 1;
}


