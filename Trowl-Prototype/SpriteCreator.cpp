#include "SpriteCreator.h"
#include <SFML\Graphics.hpp>
#include <iostream>
#include "Engine.h"

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-graphics.lib")
#endif

SpriteCreator::SpriteCreator() {
	spr = nullptr;
	tex = nullptr;
	posX = 0;
	posY = 0;
	width = 0;
	height = 0;
	rotation = 0;
}

SpriteCreator::~SpriteCreator() {
	delete spr;
	spr = nullptr;

	delete tex;
	tex = nullptr;
}

void SpriteCreator::CreateSprite(sf::Sprite &sprite, sf::Texture &texture, string source, float x = 0, float y = 0, float scaleX = 1, float scaleY = 1, float originX = 0, float originY = 0, float rotation = 0) {


	texture.loadFromFile(source);

	sprite.setTexture(texture);

	sprite.setOrigin(originX, originY);
	sprite.setPosition(x, y);
	sprite.setScale(scaleX, scaleY);
	sprite.setRotation(rotation);

	spr = &sprite;
	tex = &texture;
	posX = x;
	posY = y;
	width = scaleX;
	height = scaleY;
	this->rotation = rotation;
}

void SpriteCreator::Update(float DeltaTime) {

}

void SpriteCreator::SetScale(sf::Sprite &sprite, float x, float y) {
	width = x;
	height = y;

	sprite.setScale(x, y);
}

void SpriteCreator::SetRotation(sf::Sprite &sprite, float rot) {
	rotation = rot;

	sprite.setRotation(rot);
}

void SpriteCreator::SetOrigin(sf::Sprite &sprite, float x, float y) {
	sprite.setOrigin(x, y);
}

void SpriteCreator::SetPos(sf::Sprite &sprite, float x, float y) {
	sprite.setPosition(x, y);
	
	posX = x;
	posY = y;
}

void SpriteCreator::ChangeSprite(sf::Sprite &sprite, sf::Texture &texture, string source) {
	spr = &sprite;
	tex = &texture;

	texture.loadFromFile(source);

	sprite.setTexture(texture);
}

float SpriteCreator::GetPos(bool getX) {
	if (getX)
		return posX;
	else if (!getX)
		return posY;
}

float SpriteCreator::GetRotation() {
	return rotation;
}

float SpriteCreator::GetScale(bool getX) {
	if (getX)
		return width;
	else if (!getX)
		return height;
}