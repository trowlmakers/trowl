#pragma once
#include "PowerupManager.h"

#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML/Audio.hpp>

#include "baseState.h"
#include "Player.h"		

#include "basepowerup.h"
#include "Megaphone.h"
#include "SlowTime.h"
#include "GoldenFeather.h"


PowerupManager::PowerupManager() {} 
PowerupManager::~PowerupManager() {}

void PowerupManager::Initialize(System * inSystem)
{
	system = inSystem;

	megaPickupSound.loadFromFile("../assets/AUDIO/PowerUpMegaphone.wav");
	slowPickupSound.loadFromFile("../assets/AUDIO/PowerUpPocketWatch.wav");
	goldenPickupSound.loadFromFile("../assets/AUDIO/PowerUpGoldenFeather.wav");

}

void PowerupManager::Create(int selector, float xPos, float yPos)
{
	switch (selector)
	{
	case 1:
	{
		Megaphone* insert = new Megaphone(xPos, yPos);
		activepowerups.push_back(insert);  
	}
	break;

	case 2:
	{
		SlowTime* insert = new SlowTime(xPos, yPos);
		activepowerups.push_back(insert);
	}
	break;

	case 3:
	{
		GoldenFeather* insert = new GoldenFeather(xPos, yPos);
		activepowerups.push_back(insert);
	}
	break;

	}
}

void PowerupManager::Update(float deltatime)
{
	if (countDownTimer > 0)
	{
		countDownTimer -= deltatime;
		if (countDownTimer <= 0) 
		{
			activePowerUp = 0;
		}
	}

	if (activePowerUp != 0) { std::cout << "active powerup: " << activePowerUp << std::endl; }
															   
	for (int i = 0; i < activepowerups.size(); i++) {
		activepowerups[i]->Update(deltatime);
		if (system->player->getRect()->intersects(*activepowerups[i]->getRect()))
		{
			switch (activepowerups[i]->getType())
			{
			case 1:			 //megaP
				soundOut.setBuffer(megaPickupSound);
				activePowerUp = 1;
				countDownTimer = 5.0f;
				break;
			case 2:			  //slowTime, halved timer because time updates half as fast
				soundOut.setBuffer(slowPickupSound);
				activePowerUp = 2;
				countDownTimer = 2.5f;
				break;
			case 3:			 //GoldenFeather
				soundOut.setBuffer(goldenPickupSound);
				activePowerUp = 3;
				countDownTimer = 5.0f;
				break;
			}
			soundOut.play();	  
			//activepowerups[i]->FadeOut();

			inactivepowerups.push_back(activepowerups[i]);
			activepowerups.erase(activepowerups.begin() + i);
			i--;
		}
	}
}

void PowerupManager::Draw()
{						   
	for (int i = 0; i < activepowerups.size(); i++)
	{
		if (activepowerups[i]->getSprite() != nullptr)
		{
			system->window.draw(*activepowerups[i]->getSprite());
		}
	}
}
int PowerupManager::CheckPowerUp()
{
	return activePowerUp;
}

void PowerupManager::SetPowerUp(int selector)
{
	activePowerUp = selector;
	countDownTimer = 4;
}