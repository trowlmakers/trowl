#include "Hawk.h"
#include "AnimatedSprite.h"

#include <iostream>

#define PI 3.14159265

Hawk::Hawk() {}
Hawk::~Hawk() {}

Hawk::Hawk(float xPos, float yPos) {

	pos.x = xPos;
	pos.y = yPos;

	idleSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/hawk_sprite_sheet.png",
		"../assets/AnimatedObjects/hawk_sprite_sheet.txt");
	hitstunSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/hawk_hit_sprite_sheet.png",
		"../assets/AnimatedObjects/hawk_hit_sprite_sheet.txt");
	elecSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/hawk_elec_sprite_sheet.png",
		"../assets/AnimatedObjects/hawk_elec_sprite_sheet.txt");

	hitSound.loadFromFile("../assets/AUDIO/EnemyHawkHit.wav");
	fleeSound.loadFromFile("../assets/AUDIO/EnemyRetreat.wav");	 
	elecSound.loadFromFile("../assets/AUDIO/Electrocution.wav");

	sprite = idleSprite->getSprite();

	rect = new sf::FloatRect(
		xPos,
		yPos + 70,
		sprite->getLocalBounds().width,
		35);

	state = 0;
	internalTimer = 0;
	colliding = true;
	hitPoints = 2;
	speed = 400;
	movementTracker = 0;
	speed = 400;
}
					 
void Hawk::Activate(float xPos, float yPos)
{
	pos.x = xPos;
	pos.y = yPos;
	
	rect->left = xPos;
	rect->top = yPos + 70;
	rect->width = sprite->getLocalBounds().width;
	rect->height = 35;				 

	state = 0;
	internalTimer = 0;
	colliding = true;
	speed = 400;
	hitPoints = 2;
	movementTracker = 0;
	internalTimer = 0;
}

void Hawk::Update(float deltatime)	{
	switch (state)
	{
	case 0: //normal
		idleSprite->update(deltatime);
		sprite = idleSprite->getSprite();

		movementTracker += 5 * deltatime;
		if (movementTracker > 2 * PI) { movementTracker -= 2 * PI; }
		pos.x -= speed*deltatime;
		pos.y -= speed*deltatime*sin(movementTracker);
		rect->top = pos.y + 85;

		break;
			case 1:				  //fleeing

				//idleSprite->update(deltatime);
				//sprite = idleSprite->getSprite();

		pos.x -= speed* deltatime;
		pos.y -= speed* deltatime;		
		break;
	case 2:						   //hitstun
		hitstunSprite->update(deltatime);
		sprite = hitstunSprite->getSprite();

		internalTimer -= deltatime;
		pos.x += internalTimer * 20;

		if (internalTimer < 0) 
		{
			if (hitPoints > 0)
			{
				state = 0;
			}
			else 
			{
				soundOut.setBuffer(fleeSound);
				soundOut.play();
				state = 1;
				colliding = false;
			}
		}
		break;		  
	case 3:			  //electricity hitstun
		internalTimer -= deltatime;
		if (internalTimer <= 0)
		{
			pos.x = -50;
			state = 1;
		}	  
		elecSprite->update(deltatime);
		sprite = elecSprite->getSprite();
	}
}

sf::FloatRect * Hawk::getRect()
{
	rect->left = pos.x;
	return rect;
}

sf::Sprite * Hawk::getSprite()
{
	sprite->setPosition(pos.x, pos.y);
	return sprite;
}

void Hawk::hit(float power)
{
	soundOut.setBuffer(hitSound);
	soundOut.play();
	hitPoints -= power;
	state = 2;
	internalTimer = power / 2;
}

void Hawk::hitLightning() 
{
	if (state != 3)
	{
		internalTimer = 0.78;
	}

	state = 3;
}
void Hawk::hitBranch() 
{					  
	soundOut.setBuffer(hitSound);
	soundOut.play();
	hitPoints -= 3;
	state = 2;
	internalTimer = 0.5;
}

bool Hawk::collides()
{
	return colliding;
}

int Hawk::getType() { return 3; }

void Hawk::flee() 
{ 
	state = 1;
	colliding = false; 
}
