#include "WindParticle.h"

#include "baseParticle.h"

#include <SFML\Graphics.hpp>
#include <SFML\System.hpp  >
#include <iostream>

#include "AnimatedSprite.h"

WindParticle::~WindParticle() {}
WindParticle::WindParticle()
{
	sprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Wind_sprsheet.png",
		"../assets/AnimatedObjects/Wind_sprsheet.txt");

	rect = new sf::FloatRect(0, 0,
		sprite->getSprite()->getLocalBounds().width,
		sprite->getSprite()->getLocalBounds().height);

	aliveTime = 0.6;
}

void WindParticle::SetPosition(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
	aliveTimer = 0;
}

void WindParticle::Update(float deltatime)
{
	sprite->update(deltatime);
	aliveTimer += deltatime;
	if (aliveTimer > aliveTime)
	{
		SetPosition(-500, -500);
		sprite->reset();
	}
}

sf::FloatRect* WindParticle::GetRect()
{
	return rect;
}

sf::Sprite* WindParticle::GetSprite()
{
	sprite->setAnimationPosition(rect->left, rect->top);
	return sprite->getSprite();
}

int WindParticle::GetType()
{
	return 3;
}