#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

#include "baseEnemy.h"			

class AnimatedSprite;

class Hawk  : public baseEnemy
{
public:
	Hawk();
	~Hawk();

	Hawk(float xPos, float yPos);
	void Activate(float xPos, float yPos);

	void Update(float deltatime);
	
	sf::FloatRect* getRect();
	sf::Sprite* getSprite();

	void hit(float power);
	void hitLightning();
	void hitBranch();
	bool collides();

	int getType();

	void flee();

private:					  
	sf::Vector2f pos;

	sf::Sound soundOut;
	sf::SoundBuffer hitSound;
	sf::SoundBuffer fleeSound;
	sf::SoundBuffer elecSound;

	AnimatedSprite* idleSprite;
	AnimatedSprite* hitstunSprite;
	AnimatedSprite* elecSprite;
	sf::Texture privateTexture;
	sf::Sprite* sprite;
	sf::FloatRect* rect;
	float hitPoints;
	float speed;
	bool colliding;
	int state;
	float internalTimer;
	float movementTracker;
};

