#pragma once
#include <vector>
#include <SFML\Graphics.hpp>


class Player;

class baseProjectile
{
public:
	virtual ~baseProjectile() {};
	virtual void Update(float deltaTime) = 0;

	virtual sf::FloatRect* getRect() = 0;
	virtual sf::Sprite* getSprite() = 0;

	virtual void activate(float xPos, float yPos) = 0;

	virtual int getType() = 0;
	virtual void setPower(float powerIn) = 0;
	virtual float getPower() = 0;
};



