#include "TileCreator.h"
#include <iostream>
#include <string>
#include <SFML\Graphics.hpp>
#include <fstream>
#include "baseState.h"

TileCreator::TileCreator(float inputPosX, float inputPosY, int inputTileSizeX, int inputTileSizeY, std::string inputSource){
	posX = inputPosX;
	posY = inputPosY;
	tileX = inputTileSizeX;
	tileY = inputTileSizeY;

	color = sf::Color(255, 255, 255, 255);

	std::ifstream openfile(inputSource);

	if (openfile.is_open()) {
		std::string tileLocation;
		openfile >> tileLocation;
		tileTexture.loadFromFile(tileLocation);
		tiles.setTexture(tileTexture);

		while (!openfile.eof())
		{
			std::string str;
			openfile >> str;
			char x = str[0], y = str[2];

			if (!isdigit(x) || !isdigit(y)) {
				map[loadCounter.x][loadCounter.y] = sf::Vector2i(-1, -1);
			}
			else
				map[loadCounter.x][loadCounter.y] = sf::Vector2i(x - '0', y - '0');

			if (openfile.peek() == '\n') {
				loadCounter.x = 0;
				loadCounter.y++;
			}

			else
			{
				loadCounter.x++;
			}
		}
		loadCounter.y++;
	}
}

TileCreator::~TileCreator(){

}

void TileCreator::Draw(System* inputSystem){
	sf::View currView = inputSystem->window.getView();
	sf::FloatRect viewRect(currView.getCenter() - currView.getSize() / 2.f, currView.getSize());

	for (int i = 0; i < loadCounter.x; i++) {
		for (int j = 0; j < loadCounter.y; j++) {
			if (map[i][j].x != -1 && map[i][j].y != -1) {
				tiles.setPosition(i * tileX + posX, j * tileY + inputSystem->screenHeight - posY);
				tiles.setTextureRect(sf::IntRect(map[i][j].x * tileX, map[i][j].y * tileY, tileX, tileY));
				
				if (viewRect.contains(tiles.getPosition().x + tileX, tiles.getPosition().y) || viewRect.contains(tiles.getPosition().x - tileX, tiles.getPosition().y)) {
					tiles.setColor(color);
					inputSystem->window.draw(tiles);
				}
			}
		}
	}
}

void TileCreator::ChangePos(float inputPosX, float inputPosY) {
	posX = inputPosX;
	posY = inputPosY;
}

float TileCreator::GetX() {
	return posX;
}

float TileCreator::GetY() {
	return posY;
}

void TileCreator::SetColor(sf::Color inputColor) {
	color = inputColor;
}