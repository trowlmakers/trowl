#pragma once

#include <SFML\Graphics.hpp>
#include "baseParticle.h"

class Leaf : public baseParticle
{
public:
	Leaf();
	~Leaf();

	void SetPosition(float xPos, float yPos);

	void Update(float deltatime);

	sf::FloatRect* GetRect();
	sf::Sprite* GetSprite();

	int GetType();

private:
	sf::Texture privateTexture;
	sf::Sprite* sprite;
	sf::FloatRect* rect;
	float spreadYDir;
};

