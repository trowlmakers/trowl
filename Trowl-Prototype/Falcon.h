#pragma once
#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
#include "baseEnemy.h"	
#include "AnimatedSprite.h"

class Player;

class Falcon : public baseEnemy
{
public:
	Falcon();
	~Falcon();

	Falcon(float xPos, float yPos);
	void Activate(float xPos, float yPos);		   
	void Update(float deltatime);
	void activate();

	sf::FloatRect* getRect();
	sf::Sprite* getSprite();
	int getType();
	bool collides();

	void hit(float power); 
	void hitLightning(); 
	void hitBranch();	

	void setPlayer(Player * playerIn);

	void flee();		 
private:
	sf::Vector2f pos;

	sf::Sound soundOut;
	sf::SoundBuffer hitSound;
	sf::SoundBuffer fleeSound;
	sf::SoundBuffer elecSound;
	sf::SoundBuffer diveSound;

	sf::Texture privateTexture;
	AnimatedSprite* divingSprite;
	AnimatedSprite* hitSprite;
	AnimatedSprite* idleSprite;
	AnimatedSprite* elecSprite;
	sf::Sprite* sprite;

	Player* player;
	sf::FloatRect* rect;
	float hitPoints;
	float speed;
	bool colliding;
	int state;
	float internalTimer;
};

