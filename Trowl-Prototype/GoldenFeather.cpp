#pragma once
#include "GoldenFeather.h"
#include <iostream>
#include "SFML\Graphics.hpp"
#include "AnimatedSprite.h"

GoldenFeather::GoldenFeather()
{
	std::cout << "GoldenFeather Created Using Wrong Constructor" << std::endl;
}


//GoldenFeather::~GoldenFeather() {}

GoldenFeather::GoldenFeather(float xPos, float yPos) {
	sprite = new AnimatedSprite(
		"../assets/AnimatedObjects/PowerUpGoldenFeatherSpriteSheet.png",
		"../assets/AnimatedObjects/PowerUpGoldenFeatherSpriteSheet.txt");
	sprite->setAnimationPosition(xPos, yPos);

	rect = new sf::FloatRect(
		xPos,
		yPos,
		sprite->getSprite()->getLocalBounds().width,
		sprite->getSprite()->getLocalBounds().height);

	internalTimer = 0;

}

void GoldenFeather::Update(float deltaTime)
{
	sprite->update(deltaTime);
	if (fading) { internalTimer += deltaTime; }
}

sf::FloatRect * GoldenFeather::getRect()
{
	return rect;
}

sf::Sprite * GoldenFeather::getSprite()
{
	sf::Sprite* retSprite = sprite->getSprite();
	if (fading)
		return sprite->getSprite();
	else
		return nullptr;
}

int GoldenFeather::getType()
{
	return 3;
}

void GoldenFeather::FadeOut()
{
	fading = true;
}