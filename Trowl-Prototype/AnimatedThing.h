#pragma once

#include <SFML\Graphics.hpp>

class AnimatedSprite;

class AnimatedThing
{
public:
	AnimatedThing();
	~AnimatedThing();

	sf::Sprite* getSprite();

	void update(float deltatime);


private:
	AnimatedSprite* animated;


};

