#include "Tunnel.h"
#include <iostream>

#include <SFML\Graphics.hpp>

Tunnel::Tunnel() { std::cout << "Tunnel created using wrong constructor" << std::endl; }
Tunnel::~Tunnel() {	}

Tunnel::Tunnel(float xPos, float yPos) {
	privateTexture.loadFromFile("../assets/SimpleImages/Mountainpass.png");
	sprite = new sf::Sprite();
	sprite->setTexture(privateTexture);
	sprite->setPosition(0, 0);

	rect = new sf::FloatRect(
		xPos,
		yPos,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);
}

void Tunnel::Activate(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
}

sf::Sprite* Tunnel::GetSprite() {
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

sf::FloatRect* Tunnel::GetRect() { return rect; }

void Tunnel::Update(float deltaTime) { rect->left -= deltaTime * 500; }

int Tunnel::GetType()
{
	return 4;
}

void Tunnel::SetAudioOffset(float value)
{
}
