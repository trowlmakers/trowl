#include "TunnelWarning.h"
#include "AnimatedSprite.h"
#include <iostream>

#include <SFML\Graphics.hpp>

TunnelWarning::TunnelWarning() { std::cout << "TunnelWarning created using wrong constructor" << std::endl; }
TunnelWarning::~TunnelWarning() {	
	warning.stop();
}

void TunnelWarning::Activate(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
}

TunnelWarning::TunnelWarning(float xPos, float yPos) {
	sprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Warning_sign_sprsheet.png",
		"../assets/AnimatedObjects/Warning_sign_sprsheet.txt");

	rect = new sf::FloatRect(
		xPos,
		yPos,
		0, 0);

	speed = 700;

	if (warning.getStatus() != 3)
	{
		warning.openFromFile("../assets/AUDIO/WarningTunnel_edit.wav");
		warning.setLoop(false);
		warning.setVolume(60);
		warning.setRelativeToListener(false);
		warning.play();
	}
}			   

sf::Sprite* TunnelWarning::GetSprite() {
	retSprite->setPosition(rect->left, rect->top);

	return retSprite;
}

sf::FloatRect* TunnelWarning::GetRect() { return rect; }

void TunnelWarning::Update(float deltaTime) { 
	rect->left -= deltaTime * speed; 

	sprite->update(deltaTime);
	retSprite = sprite->getSprite();
}

int TunnelWarning::GetType()
{
	return 5;
}

void TunnelWarning::SetAudioOffset(float value) {
	float reCalc = 125 - abs(value / 10);
	warning.setVolume(reCalc);
}