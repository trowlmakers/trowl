#include "PlayerHead.h"
#include "AnimatedSprite.h"

#include <iostream>

#include <SFML\Graphics.hpp>


PlayerHead::PlayerHead()
{
	sprite = new sf::Sprite;
	normalTexture.loadFromFile("../assets/SimpleImages/Owl_Head_White.png");
	sprite->setTexture(normalTexture);

	goldenTexture.loadFromFile("../assets/SimpleImages/Owl_Head_Gold.png");

	screamingSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Owl_scream_sprsheet.png",
		"../assets/AnimatedObjects/Owl_scream_sprsheet.txt"
		);
	state = 0;
	internalTimer = 0;
}					  

PlayerHead::~PlayerHead(){}

void PlayerHead::setRed(int redVal)
{
	sprite->setColor(sf::Color(255, 255 - redVal / 1.75, 255 - redVal / 1.75, 255));
}

void PlayerHead::setHeadPosition(float xPos, float yPos)
{
        	sprite->setPosition(xPos, yPos);
}
								    
sf::Sprite* PlayerHead::getSprite()
{
	return sprite;
}

void PlayerHead::setGolden(bool set)
{
	golden = set;
}

void PlayerHead::Update(float deltaTime) {
	switch (state) {
	case 0:			 //default state
       	if (golden)
		{
			sprite->setTexture(goldenTexture);
		}
		else
		{
			sprite->setTexture(normalTexture);
		}
		break;

	case 1:			//screaming state
 		screamingSprite->update(deltaTime);
		sprite = screamingSprite->getSprite();
		internalTimer -= deltaTime;
		if (internalTimer <= 0)
		{
			state = 0;
			screamingSprite->reset();
		}												   
		break;
	}
}

void PlayerHead::Scream() {
	state = 1;
	internalTimer = 1;
}