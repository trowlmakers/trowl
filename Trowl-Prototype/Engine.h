#pragma once
#include "SFML\System.hpp"

class baseState;
class RenderWindow;
struct System;

class Engine
{
public:
	Engine();
	~Engine();
	bool Engine::Initialize();
	bool Engine::Update();
	void Engine::Shutdown();
	void Restart();
	void LoadingImage();
private:
	float frameTime;
	float timeAccumulator;
	float targetTime;
	sf::Clock clock;
	baseState* activeState;
	System* system;
};	


