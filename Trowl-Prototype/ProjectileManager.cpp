#include "ProjectileManager.h"

#include "Player.h"

#include "baseProjectile.h"
#include "PlayerProjectile.h"
#include "HumanProjectile.h"
#include "ProjectileManager.h"

#include <iostream>
#include <math.h>

#include "baseState.h" //for system struct

ProjectileManager::ProjectileManager() {}
ProjectileManager::~ProjectileManager() {}

void ProjectileManager::Initialize(System* inSystem) { system = inSystem; }	

void ProjectileManager::Update(float deltaTime) {
	for (int i = 0; i < activeProjectiles.size(); i++) {
		activeProjectiles[i]->Update(deltaTime);
		if (system->player->getRect()->left + system->screenWidth < activeProjectiles[i]->getRect()->left ||
			system->player->getRect()->left - system->screenWidth > activeProjectiles[i]->getRect()->left ||
			activeProjectiles[i]->getRect()->top > system->screenHeight ||
			activeProjectiles[i]->getRect()->top < 0 - activeProjectiles[i]->getRect()->height ||
			activeProjectiles[i]->getPower() < 0)
		{	  //if out of bounds, or power too low, clean up
			inactiveProjectiles.push_back(activeProjectiles[i]);
			activeProjectiles.erase(activeProjectiles.begin() + i);
			i--;
			std::cout << "projectile put into pool" << std::endl;
		}
	}
}

void ProjectileManager::Create(int selector, float xPos, float yPos, float powerIn)
{
	switch (selector)
	{
	case 1:
	{
		bool found = false;

		for (int i = 0; i < inactiveProjectiles.size(); i++) {
			if (inactiveProjectiles[i]->getType() == 1) {
				inactiveProjectiles[i]->activate(xPos + system->player->getRect()->width, yPos);
				inactiveProjectiles[i]->setPower(powerIn);

				activeProjectiles.push_back(inactiveProjectiles[i]);
				inactiveProjectiles.erase(inactiveProjectiles.begin() + i);

				std::cout << "projectile grabbed from pool" << std::endl;
				found = true;
				break;
			}
		}
		if (found == false)
		{
			PlayerProjectile* insert = new PlayerProjectile(xPos + system->player->getRect()->width, yPos, system);
			insert->setPower(powerIn);

			activeProjectiles.push_back(insert);
			std::cout << "PlayerProjectile created" << std::endl;
		}
	}	

	break;
	case 2:
	{
		HumanProjectile* insert = new HumanProjectile(xPos, yPos, CalculateAngle(xPos, yPos));
		insert->setPower(1);
		activeProjectiles.push_back(insert);
	}
	break;
	}
}


void ProjectileManager::Draw() {
	for (int i = 0; i < activeProjectiles.size(); i++) {
		system->window.draw(*activeProjectiles[i]->getSprite());
	}
}

void ProjectileManager::checkCollisions() {
	for (int i = 0; i < activeProjectiles.size(); i++) {
		if (activeProjectiles[i]->getType() == 1) {
			if (system->enemyManager->HitBy(*activeProjectiles[i]->getRect(), activeProjectiles[i]->getPower())) {

				system->player->decreaseStress(activeProjectiles[i]->getPower() / 3);

				inactiveProjectiles.push_back(activeProjectiles[i]);
				activeProjectiles.erase(activeProjectiles.begin() + i);
				i--;
				std::cout << "projectile put into pool" << std::endl;

			}	  				
		}
		else if (activeProjectiles[i]->getType() == 2) {
			if (system->player->getRect()->intersects(*activeProjectiles[i]->getRect())) {
				
				system->player->hitByPhysical();

				inactiveProjectiles.push_back(activeProjectiles[i]);
				activeProjectiles.erase(activeProjectiles.begin() + i);
				i--;
				std::cout << "projectile put into pool" << std::endl;
			}														 
		}
	}
}

float ProjectileManager::CalculateAngle(float xPos, float yPos) {
	float deltaX = system->player->getRect()->left - xPos, 
		deltaY = system->player->getRect()->top - yPos,
		angle = atan2(deltaY - 100, deltaX);

	return angle;
}

void ProjectileManager::Erase() {
	for (auto itr = activeProjectiles.begin(); itr != activeProjectiles.end(); )
	{
		inactiveProjectiles.push_back(*itr);
		itr = activeProjectiles.erase(itr);
	}
}