#pragma once

#include <SFML\Graphics.hpp>
#include <iostream>

using namespace std;
using namespace sf;

class Sprite;
class Texture;

class SpriteCreator {
public:
	SpriteCreator();
	~SpriteCreator();

	//Sprite, Texture, Texture location, Position X, Position Y, Scale X, Scale Y, Origin X, Origin Y, Rotation
	void CreateSprite(sf::Sprite &sprite, sf::Texture &texture, string source, float posX, float posY, float scaleX, float scaleY, float originX, float originY, float rotation);

	void Update(float DeltaTime);

	void SetScale(sf::Sprite &sprite, float x, float y);
	void SetRotation(sf::Sprite &sprite, float rot);
	void SetOrigin(sf::Sprite &sprite, float x, float y);
	void SetPos(sf::Sprite &sprite, float x, float y);
	void ChangeSprite(sf::Sprite &sprite, sf::Texture &texture, string source);

	float GetPos(bool getX);
	float GetRotation();
	float GetScale(bool getX);

private:
	sf::Texture* tex;
	sf::Sprite* spr;

	float posX;
	float posY;
	float width;
	float height;
	float rotation;
};