#pragma once
#include "baseObstacle.h"
#include <SFML\Graphics.hpp>  
#include <SFML\Audio.hpp>
#include <SFML\System\Vector2.hpp>

class AnimatedSprite;

class ThunderCloud : public baseObstacle
{
public:
	ThunderCloud();
	ThunderCloud(float xPos, float yPos);
	~ThunderCloud();

	void Activate(float xPos, float yPos);

	sf::Sprite* GetSprite();
	sf::FloatRect* GetRect();

	void Update(float deltaTime);

	int GetType();

	void Draw(System * system);

	void SetAudioOffset(float value);


private:
	sf::Sound soundOut;
	sf::SoundBuffer thunderSound;

	sf::Vector2f pos;

	sf::Sprite* retSprite;
	AnimatedSprite* windUpSprite;
	AnimatedSprite* lightningSprite;
	AnimatedSprite* windDownSprite;
	sf::Sprite* idleSprite;	
	sf::Texture idleTexture;

	sf::FloatRect* rect;	 
	int state;
	float internalTimer;
};

