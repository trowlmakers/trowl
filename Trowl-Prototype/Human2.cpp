#include "Human2.h"
#include "Player.h"

#include "AnimatedSprite.h"
#include "baseState.h"

#include <iostream>

Human2::Human2() {}
Human2::~Human2() {}

Human2::Human2(float xPos, float yPos, System* systemIn) {
	system = systemIn;
	speed = 0;

	//throwingSprite = AnimatedSprite();
	int i = rand() % 2;
	if (i == 0) {
		idleTexture.loadFromFile("../assets/simpleImages/Hooman4.2.png");
		throwingSprite = new AnimatedSprite(
			"../assets/AnimatedObjects/Human4ChargeUp_SpSh.png",
			"../assets/AnimatedObjects/Human4ChargeUp_SpSh.txt");
		windDownSprite = new AnimatedSprite(
			"../assets/AnimatedObjects/Human4ThrowRelease_SpSH.png",
			"../assets/AnimatedObjects/Human4ThrowRelease_SpSH.txt");
	}
	else {
		idleTexture.loadFromFile("../assets/simpleImages/Hooman5.2.png");
		throwingSprite = new AnimatedSprite(
			"../assets/AnimatedObjects/Human5ChargeUp_SpSh.png",
			"../assets/AnimatedObjects/Human5ChargeUp_SpSh.txt");
		windDownSprite = new AnimatedSprite(
			"../assets/AnimatedObjects/Human5ThrowRelease_SpSH.png",
			"../assets/AnimatedObjects/Human5ThrowRelease_SpSH.txt");
	}



	idleSprite = new sf::Sprite;
	idleSprite->setTexture(idleTexture);
	idleSprite->setPosition(0, 0);
	sprite = idleSprite;


	rect = new sf::FloatRect(xPos, yPos,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	state = 0;
	internalTimer = 0;
	throwingTimer = 0;
	throwingFrequency = 3;
	colliding = false;

}
void Human2::Activate(float xPos, float yPos)
{
	rect = new sf::FloatRect(xPos, yPos,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	state = 0;
	internalTimer = 0;
	throwingTimer = 0;
	throwingFrequency = 3;
	colliding = false;
}
void Human2::Update(float deltatime) {
	switch (state)
	{
	case 0: //idle
	{
		throwingTimer += deltatime;
		if (throwingTimer > throwingFrequency) {
			throwingTimer -= throwingFrequency + rand() % 5;
			state = 1;
		}
	}
	break;
	case 1: //throwing 
		throwingSprite->update(deltatime);
		sprite = throwingSprite->getSprite();
		internalTimer += deltatime;
		if (internalTimer > 0.42) 
		{
			throwingSprite->reset();
			system->projectileManager->Create(2, rect->left + rect->width, rect->top,0);
			state = 2; 
			internalTimer = 0;
		}
		break;
	case 2: //windDown
		windDownSprite->update(deltatime);
		sprite = windDownSprite->getSprite();
		internalTimer += deltatime;
		if (internalTimer > 0.24)
		{
			windDownSprite->reset();
			state = 0;
			sprite = idleSprite;
			internalTimer = 0;
		}
	}
}

sf::FloatRect * Human2::getRect()
{
	return rect;
}

sf::Sprite * Human2::getSprite()
{
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

void Human2::hit(float power)
{
}

void Human2::hitLightning() {}
void Human2::hitBranch() {}

bool Human2::collides()
{
	return colliding;
}

void Human2::flee()
{
}

void Human2::activate()
{
	state = 0;
	colliding = false;
}

void Human2::setPlayer(Player* playerIn) { player = playerIn; }

int Human2::getType() { return 5; }