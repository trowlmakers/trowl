#include "OwletManager.h"
#include "Owlet.h"
#include "baseState.h"
#include "Player.h"
#include "EnemyManager.h"



OwletManager::OwletManager(){}

OwletManager::~OwletManager(){
	inactiveOwlet.clear();
	activeOwlet.clear();
	takenOwlet.clear();
}

void OwletManager::Initialize(System* inputSystem){
	system = inputSystem;

	hitbox.setFillColor(sf::Color(0, 255, 0, 125));
	hitbox.setOutlineColor(sf::Color(255, 0, 0));
	hitbox.setOutlineThickness(1);
}

void OwletManager::Create(float xPos){
	for (int i = 0; i < inactiveOwlet.size(); i++)
	{
		activeOwlet.push_back(inactiveOwlet[i]);
		inactiveOwlet[i]->Activate(xPos, system->screenHeight - 115);
		inactiveOwlet.erase(inactiveOwlet.begin() + i);
		std::cout << "Owlet pulled from pool" << std::endl;
		return;	  //if we find something to use, we dont want to keep going.
	}
	
	Owlet* insert = new Owlet("../assets/Owlet.png", xPos, system->screenHeight - 115, system);
	activeOwlet.push_back(insert);
}

void OwletManager::Update(float deltaTime){
	for (int i = 0; i < activeOwlet.size(); i++) {
		activeOwlet[i]->Update(deltaTime);

		if (system->player->getRect()->intersects(*activeOwlet[i]->getRect())) {
			std::cout << "Owlet Picked Up" << std::endl;
			system->player->decreaseStress(3);
			system->score += 100;
			takenOwlet.push_back(activeOwlet[i]);
			activeOwlet[i]->ChangeState(owletsTaken);
			owletsTaken++;
		}
	}
}

bool OwletManager::CollidesWith(sf::FloatRect, int){
	return false;
}

void OwletManager::Draw(){
	for(int i = 0; i < activeOwlet.size(); i++) {
		system->window.draw(*activeOwlet[i]->getSprite());
	}
}

void OwletManager::DrawHitBoxes(){
	for (int i = 0; i < activeOwlet.size(); i++) {
		//Set drawRect position and size then draw
		hitbox.setPosition(sf::Vector2f(
			activeOwlet[i]->getRect()->left,
			activeOwlet[i]->getRect()->top));
		hitbox.setSize(sf::Vector2f(
			activeOwlet[i]->getRect()->width,
			activeOwlet[i]->getRect()->height));

		system->window.draw(hitbox);
	}
}

void OwletManager::PutInPool(){
	Owlet* insert = new Owlet("../assets/Owlet.png", 0, system->screenHeight - 115, system);
	inactiveOwlet.push_back(insert);
}

void OwletManager::ObstacleIn(sf::FloatRect, int){

}

int OwletManager::GetOwlets(){
	return owletsTaken;
}

Owlet* OwletManager::GetFollowObj(int i) {
	return takenOwlet[i - 1];
}