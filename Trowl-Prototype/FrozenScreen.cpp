#include "FrozenScreen.h"

#include <SFML\Graphics.hpp>
#include <string>
#include "baseState.h"

FrozenScreen::FrozenScreen() {}
FrozenScreen::~FrozenScreen()		{}

void FrozenScreen::Initialize(System* systemIn) 
{
	system = systemIn; 
}

void FrozenScreen::Freeze(std::string file) 
{
	internalTexture.loadFromFile(file);
	spriteOut.setTexture(internalTexture);

	system->window.clear();
	system->window.draw(spriteOut);
	system->window.display();
	sf::Event event;
	bool loop = true;
	// while there are pending events...
	while (system->window.pollEvent(event) || loop)
	{
		// check the type of the event...
		switch (event.type)
		{
			// window closed
		case sf::Event::Closed:
			system->window.close();
			loop = false;
			break;

			//Check for Keys pressed
		case sf::Event::KeyPressed:
			switch (event.key.code) {

				//Quit Program
			case sf::Keyboard::Escape:
				system->window.close();
				loop = false;
				break;

			case sf::Keyboard::Space:
				loop = false;
				break;
			}

		default:
			continue;
		}
	}
}

