#pragma once
#include "basepowerup.h"
#include "SFML/Graphics.hpp"

class AnimatedSprite;

class Wind : public basePowerUp
{

public:
	Wind();
	Wind(float xPos, float yPos);
	~Wind() {};
	void Update(float deltaTime);

	sf::FloatRect* getRect();
	sf::Sprite* getSprite();

	int getType();

private:
	sf::FloatRect* rect;


};

