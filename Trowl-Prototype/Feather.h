#pragma once

#include <SFML\Graphics.hpp>
#include "baseParticle.h"

class Feather : public baseParticle
{
public:
	Feather();
	~Feather();

	void SetPosition(float xPos, float yPos);

	void Update(float deltatime);

	sf::FloatRect* GetRect();
	sf::Sprite* GetSprite();

	int GetType();

private:
	sf::Texture privateTexture;
	sf::Sprite* sprite;
	sf::FloatRect* rect;
	float aliveTimer;
	float speedX;
	float speedY;
};

