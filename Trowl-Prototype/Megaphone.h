#pragma once
#include "basepowerup.h"
#include "SFML/Graphics.hpp"

class AnimatedSprite;

class Megaphone : public basePowerUp
{

public:
	Megaphone();
	Megaphone(float xPos, float yPos);
	~Megaphone() {};
	void Update(float deltaTime);

	sf::FloatRect* getRect();
	 sf::Sprite* getSprite();

	 int getType();

private:
	AnimatedSprite* sprite;
	sf::FloatRect* rect;

	
};

