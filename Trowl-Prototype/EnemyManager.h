#pragma once

#include <SFML\Graphics.hpp>

struct System;
class baseEnemy;
class Player;

class EnemyManager
{
public:
	EnemyManager();
	~EnemyManager();								  
	void Initialize(System * inSystem);

	void Create(int selector, float xPos, float yPos);

	void Update(float deltaTime);

	bool HitBy(sf::FloatRect collisionCheck, float power);
	bool CollidesWith(sf::FloatRect);

	void Draw();		  
	void DrawHitBoxes();

	void putInPool(int selector);

	void obstacleIn(sf::FloatRect* rectIn, int type);

	void Erase();


private:
	System* system;

	sf::RectangleShape drawRect;

	std::vector<baseEnemy*> activeEnemies;
	std::vector<baseEnemy*> inactiveEnemies;
};

