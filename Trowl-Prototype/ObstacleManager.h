#pragma once

#include <SFML\Graphics.hpp>

struct System;
class baseObstacle;

class ObstacleManager
{
public:
	ObstacleManager();
	~ObstacleManager();

	void Initialize(System * inSystem);

	void Create(int selector, float xPos, float yPos);

	bool Update(float deltaTime);

	void Draw();

	void DrawTrees();

	void Clear();

	void DrawHitBoxes();

	void DrawWarning();

	void Erase();

private:
	sf::RectangleShape drawRect;

	System* system;

	std::vector<baseObstacle*> activeObstacles;
	std::vector<baseObstacle*> inactiveObstacles;

	sf::Sprite spriteOut;
	sf::Texture tunnelWarningArrow;
	float movementTracker;

};

