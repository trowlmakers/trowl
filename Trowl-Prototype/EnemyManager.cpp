#include "EnemyManager.h"

#include "AlphaEnemy.h"
#include "Hawk.h"
#include "Eagle.h"
#include "Falcon.h"
#include "Human.h"
#include "Human2.h"

#include "Player.h"
#include "Owlet.h"

#include "baseState.h" //for system struct

#include <iostream>


EnemyManager::EnemyManager() {}
EnemyManager::~EnemyManager() {}

void EnemyManager::Initialize(System* inSystem)
{
	system = inSystem; 

	drawRect.setFillColor(sf::Color(0, 255, 0, 125));
	drawRect.setOutlineColor(sf::Color(255, 0, 0));
	drawRect.setOutlineThickness(1);

	for (int i = 1; i < 4; i++)		   //for each of our 3 enemy types
	{
		for (int u = 0; u < 4; u++)	//create 4 enemies
		{
			putInPool(i);
		}
	}					 
}

void EnemyManager::Create(int selector, float xPos, float yPos)
{
	for (int i = 0; i < inactiveEnemies.size(); i++)
	{
		if (inactiveEnemies[i]->getType() == selector)
		{
			activeEnemies.push_back(inactiveEnemies[i]);
			inactiveEnemies[i]->Activate(xPos, yPos);
			inactiveEnemies.erase(inactiveEnemies.begin() + i);
			std::cout << "enemy pulled from pool" << std::endl;
			return;	  //if we find something to use, we dont want to keep going.
		}
	}

	switch (selector)
	{
	case 0:
	{							
		AlphaEnemy* insert = new AlphaEnemy(xPos, yPos);
		activeEnemies.push_back(insert);
	}
	break;
	case 1:
	{
		Eagle* insert = new Eagle(xPos, yPos);
		activeEnemies.push_back(insert);
		insert->setPlayer(system->player);
		std::cout << "Eagle HardSpawned" << std::endl;
	}													   
	break;
	case 2:
	{
		Falcon* insert = new Falcon(xPos, yPos);
		activeEnemies.push_back(insert);
		insert->setPlayer(system->player);
		std::cout << "Falcon HardSpawned" << std::endl;
	}
	break;
	case 3:
	{
		Hawk* insert = new Hawk(xPos, yPos);
		activeEnemies.push_back(insert);
		std::cout << "Hawk HardSpawned" << std::endl;
	}
	break;
	case 4:
	{
		Human* insert = new Human(xPos, yPos, system);
		activeEnemies.push_back(insert);
	}
	break;
	case 5:
	{
		Human2* insert = new Human2(xPos, yPos, system);
		activeEnemies.push_back(insert);
	}
	break;
	}
}

void EnemyManager::Update(float deltaTime)
{

	for (int i = 1; i < 5; i++)
	{

	}
	for (int i = 0; i < activeEnemies.size(); i++)
	{
		activeEnemies[i]->Update(deltaTime);
		 
		if (activeEnemies[i]->collides())
		{
			if (system->player->getRect()->intersects(*activeEnemies[i]->getRect()))
			{
				system->player->hitByPhysical();
				activeEnemies[i]->flee();
			}
			
			/*for (int j = 0; j < system->player->GetOwlets(); j++) {
				if (system->player->GetFollowObj(j)->getRect()->intersects(*activeEnemies[i]->getRect())) {
					//ADD IN OWLET HIT FUNCTIONALITY HERE
					std::cout << "Enemy Collided with Owlet" << std::endl;
				}
			}*/
		}

		if (system->window.mapPixelToCoords(sf::Vector2i(0,0)).x > activeEnemies[i]->getRect()->left + activeEnemies[i]->getRect()->width ||
			activeEnemies[i]->getRect()->top > system->screenHeight ||
			activeEnemies[i]->getRect()->top < 0 - activeEnemies[i]->getRect()->height)
		{	  //if out of bounds backward downward or upward, clean up
			inactiveEnemies.push_back(activeEnemies[i]);
			activeEnemies.erase(activeEnemies.begin() + i);
			i--;
			std::cout << "Enemy put into pool" << std::endl;
		}

	}
}

bool EnemyManager::HitBy(sf::FloatRect collisionCheck, float power)
{
	for (int i = 0; i < activeEnemies.size(); i++) {
		if (activeEnemies[i]->getRect()->intersects(collisionCheck)) {
			if (activeEnemies[i]->collides()) {	
				activeEnemies[i]->hit(power);

				for (int u = 0; u < 10; u++) {
					system->particleManager->Create(
						2,
						activeEnemies[i]->getRect()->left,
						activeEnemies[i]->getRect()->top);
				}

					switch (activeEnemies[i]->getType()) {
				case 1:	//eagle
					system->score += 50;
					std::cout << system->score << std::endl;
					break;
				case 2:	//falcon
					system->score += 10;
					std::cout << system->score << std::endl;   				
					break;
				case 3:	//hawk
					system->score += 25;
					std::cout << system->score << std::endl;
					break;
				}			   
				return true;
			}
		}
	}
	return false;
}

bool EnemyManager::CollidesWith(sf::FloatRect collisionCheck) {
	for (int i = 0; i < activeEnemies.size(); i++) {
		if (activeEnemies[i]->getRect()->intersects(collisionCheck)) {
			if (activeEnemies[i]->collides()) {
				return true;
			}
		}
	}
	return false;									
}

void EnemyManager::Draw()
{
	for (int i = 0; i < activeEnemies.size(); i++)		
	{
		system->window.draw(*activeEnemies[i]->getSprite());
	}
}

void EnemyManager::Erase() {
	for (auto itr = activeEnemies.begin(); itr != activeEnemies.end(); )
	{
		std::cout << "Hello" << std::endl;
		inactiveEnemies.push_back(*itr);
		itr = activeEnemies.erase(itr);
	}
}

void EnemyManager::DrawHitBoxes() {

	for (int i = 0; i < activeEnemies.size(); i++) {
		//Set drawRect position and size then draw
		drawRect.setPosition(sf::Vector2f(
			activeEnemies[i]->getRect()->left,
			activeEnemies[i]->getRect()->top));
		drawRect.setSize(sf::Vector2f(
			activeEnemies[i]->getRect()->width,
			activeEnemies[i]->getRect()->height));

		system->window.draw(drawRect);
	}
}

void EnemyManager::putInPool(int selector)
{
	switch (selector)
	{
	case 1:
	{
		Eagle* insert = new Eagle(0,0);
		inactiveEnemies.push_back(insert);
		insert->setPlayer(system->player);
	}
	break;
	case 2:
	{
		Falcon* insert = new Falcon(0,0);
		inactiveEnemies.push_back(insert);
		insert->setPlayer(system->player);
	}
	break;
	case 3:
	{
		Hawk* insert = new Hawk(0,0);
		inactiveEnemies.push_back(insert);
	}
	break;
	case 4:
	{
		Human* insert = new Human(0,0, system);
		inactiveEnemies.push_back(insert);
	}
	break;
	}		
}

void EnemyManager::obstacleIn(sf::FloatRect * rectIn, int type)
{
	for (int i = 0; i < activeEnemies.size(); i++)
	{
		if (activeEnemies[i]->getRect()->intersects(*rectIn))
		{
			switch (type)
			{
			case 1:
				activeEnemies[i]->hitBranch();
				break;
			case 2:
				activeEnemies[i]->hitLightning();
				break;
			}
		}
	}
}

