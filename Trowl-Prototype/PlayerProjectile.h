#pragma once
#include "baseProjectile.h"
 #include <SFML\Graphics.hpp>

class Sprite;
struct System;

class PlayerProjectile : public baseProjectile
{
public:
	PlayerProjectile();
	~PlayerProjectile();

	PlayerProjectile(float xPos, float yPos, System* systemIn);

	void activate(float xPos, float yPos);

	void Update(float deltatime);

	sf::FloatRect* getRect();
	sf::Sprite* getSprite();
	int getType();
	void setPower(float powerIn);
	float getPower();
private:
	sf::Texture privateTexture;
	sf::FloatRect* rect;
	sf::Sprite* sprite;
	System* system;
	float speed;
	float yDir;
	float xDir;
	float power;
};

