#include "AnimatedThing.h"
#include "AnimatedSprite.h"

#include <string>
#include <SFML\Graphics.hpp>

AnimatedThing::AnimatedThing()
{
	animated = new AnimatedSprite(
		"../assets/AnimatedObjects/PowerUpPocketWatchAnimationSpriteSheet.png",
		"../assets/AnimatedObjects/PowerUpPocketWatchAnimationSpriteSheet.txt");
}					

AnimatedThing::~AnimatedThing()
{
}

sf::Sprite* AnimatedThing::getSprite()
{
	return animated->getSprite();
}

void AnimatedThing::update(float deltatime)
{
	animated->update(deltatime);
}