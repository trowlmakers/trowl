#pragma once
#include "baseProjectile.h"

#include <SFML\Graphics.hpp>

struct System;
class Sprite;

class HumanProjectile : public baseProjectile
{
public:
	HumanProjectile();
	~HumanProjectile();

	HumanProjectile(float xPos, float yPos, float angle);

	void activate(float xPos, float yPos);

	void Update(float deltatime);

	sf::Sprite* getSprite();
	sf::FloatRect* getRect();
	int getType();

	void setPower(float powerIn);
	float getPower();

private:
	sf::Texture privateTexture;
	sf::FloatRect* rect;
	sf::Sprite* sprite;
	float yVel;
	float xVel;
	float weight;
	float power;
	System* system;			   
};

