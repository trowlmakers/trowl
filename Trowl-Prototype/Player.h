#pragma once

class PlayerHead;
class AnimatedSprite;
struct System;
class Owlet;

#include <SFML/Audio.hpp>
#include <SFML\Graphics.hpp>

class Player
{
public:
	Player();
	Player(float xPos, float yPos, System* inputSystem);
	~Player();
	bool update(float deltaTime);

	void setXPos(float xPos);
	void setYPos(float yPos);

	sf::FloatRect* getRect();

	void Draw();

	void increaseStress(float increase);
	void decreaseStress(float decrease);
	float getStress();

	bool IsFacingRight();

	float GetAngle();
	float GetOwlets();
	Owlet * GetOwlet(int i);
	Owlet* GetFollowObj(int i);
	void AddOwlet(Owlet* o);

	void SetHeight(float x, float y);

	void SetMaxSide(float min, float max);

	float GetMinSide();

	float GetSpeed();

	void hitByThunder();
	void hitByPhysical();
	void hitByTunnel();

private:

	sf::Vector2f pos;

	int shootingState;
	int movementState;
	float movementTimer;
	float shootingTimer;
	
	PlayerHead* head;

	System* system;

	sf::Sound soundOut;
	sf::SoundBuffer electrocutionSound;
	sf::SoundBuffer physHitSound;
	sf::SoundBuffer shootSound;
	sf::SoundBuffer tunnelHitSound;
			
	sf::Texture privateTexture;
	sf::Sprite* sprite;
	AnimatedSprite* idleSprite;
	AnimatedSprite* idleSuperSprite;
	AnimatedSprite* lightningedSprite;
	AnimatedSprite* physHitSprite;
	sf::FloatRect* rect;

	float xVel;
	float yVel;
	float speed;
	float inertia;

	float coolDownTime;
	float coolDownCounter;			
	float stressLevel;
	bool facingRight;

	int currOwlets = 0;
	
	float maxHeight = 0;
	float minHeight = 0;
	float minSide = 0;
	float maxSide = 0;

	float angle;

	sf::Music heartBeat;
};

