#pragma once

#include <SFML\Graphics.hpp>
#include "baseState.h"

struct System;

class baseObstacle
{
public:
	virtual ~baseObstacle() {};
	virtual void Activate(float xPos, float yPos) = 0;
	virtual void Update(float deltatime) = 0;
	virtual sf::FloatRect* GetRect() = 0;
	virtual sf::Sprite* GetSprite() = 0;
	virtual int GetType() = 0;
	virtual void SetAudioOffset(float value) = 0;
};

