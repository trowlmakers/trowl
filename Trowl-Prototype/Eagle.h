#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include "baseEnemy.h"		

class AnimatedSprite;
class Player;

class Eagle : public baseEnemy
{
public:
	Eagle();
	~Eagle();

	Eagle(float xPos, float yPos);
	void Activate(float xPos, float yPos);

	void Update(float deltatime);

	sf::FloatRect* getRect();
	sf::Sprite* getSprite();

	void hit(float power);
	void hitLightning();
	void hitBranch();
	void flee();

	void activate();

	void setPlayer(Player * inPlayer);

	int getType();
	bool collides();

private:
	sf::Vector2f pos;


	sf::Sound soundOut;
	sf::SoundBuffer hitSound;
	sf::SoundBuffer fleeSound;
	sf::SoundBuffer diveSound;
	sf::SoundBuffer elecSound;

	sf::Texture privateTexture;
	AnimatedSprite* idleSprite;
	AnimatedSprite* divingSprite;
	AnimatedSprite* elecSprite;
	sf::Sprite* sprite;
	sf::FloatRect* rect;
	Player* player;
	float hitPoints;
	float speed;
	bool colliding;
	int state;
	float internalTimer;
};

