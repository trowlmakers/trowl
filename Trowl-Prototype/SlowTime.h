#pragma once
#include "basepowerup.h"
#include "SFML/Graphics.hpp"

class AnimatedSprite;

class SlowTime : public basePowerUp
{

public:
	SlowTime();
	SlowTime(float xPos, float yPos);
	~SlowTime() {};
	void Update(float deltaTime);

	sf::FloatRect* getRect();
	sf::Sprite* getSprite();

	int getType();

private:							
	AnimatedSprite* sprite;
	sf::FloatRect* rect;


};

