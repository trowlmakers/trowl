#include "InsideState.h"
#include "Engine.h"
#include "SpriteCreator.h"
#include "Player.h"
#include "StateSwitch.h"
#include "Owlet.h"
#include "Hud.h"
#include "TileCreator.h"

#include "Human.h"

#include "Camera.h"
#include "AnimatedThing.h"

#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

#include <iostream>
#include <vector>
#include <math.h>

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#pragma comment(lib, "sfml-audio.lib")
#endif

Camera* cameraInside;
Camera* staticElementsInside;

TileCreator* background;
TileCreator* background2;

TileCreator* LampParallax;

TileCreator* forground;
TileCreator* forground2;

Hud* hud;

sf::RectangleShape displayHitbox;

sf::Sprite starsIns;
sf::Texture* starsTexIns;
sf::Sprite cloud1ins;
sf::Texture* cloud1TexIns;
sf::Sprite cloud2ins;
sf::Texture* cloud2TexIns;
sf::Sprite cloud3ins;
sf::Texture* cloud3TexIns;
sf::Sprite cloud4ins;
sf::Texture* cloud4TexIns;
sf::Sprite cloud5ins;
sf::Texture* cloud5TexIns;
sf::Sprite mountainsIns;
sf::Texture* mountainsTexIns;


InsideState::InsideState(){}


InsideState::~InsideState(){}

void InsideState::Initiate(System* inputSystem) {
	system = inputSystem;

	displayHitbox.setFillColor(sf::Color(0, 255, 0, 125));
	displayHitbox.setOutlineColor(sf::Color(255, 0, 0));
	displayHitbox.setOutlineThickness(1);

	//Creating both cameras
	cameraInside = new Camera(system, system->player->getRect()->left - system->screenWidth / 2, 0); //Include player as the target for the camera to follow
	staticElementsInside = new Camera(system, 0, 0); //Don't include player since this camera is suppose to be static and only display static elements
	
	hud = new Hud();
	hud->Initialize(system);

	background = new TileCreator(-10 + xOffset, system->screenHeight - 120, 30, 30, "../assets/Tilesets/insideTrainTileMap.txt");
	background2 = new TileCreator(xOffset, system->screenHeight - 72, 30, 30, "../assets/Tilesets/insideTrainTileMap_2.txt");

	LampParallax = new TileCreator(500 + xOffset, 450, 270, 210, "../assets/SimpleImages/Tunnel_bkgrnd.txt");

	forground = new TileCreator(-10 + xOffset, system->screenHeight - 120, 30, 30, "../assets/Tilesets/insideTrainTileMap_3.txt");
	forground->SetColor(sf::Color(xOffset, 0, 0, 255));
	forground2 = new TileCreator(xOffset, system->screenHeight - 60, 30, 30, "../assets/Tilesets/insideTrainTileMap_2.txt");
	forground2->SetColor(sf::Color(xOffset, 0, 0, 255));

	//Create Background
	{
		{
			starsTexIns = new sf::Texture();
			starsTexIns->loadFromFile("../assets/SimpleImages/Background_sky.png");

			starsIns = sf::Sprite();
			starsIns.setTexture(*starsTexIns);
			starsIns.setPosition(xOffset, 0);

			sf::FloatRect spriteRect = starsIns.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			mountainsTexIns = new sf::Texture();
			mountainsTexIns->loadFromFile("../assets/SimpleImages/Background_mountains.png");

			mountainsIns = sf::Sprite();
			mountainsIns.setTexture(*mountainsTexIns);
			mountainsIns.setPosition(xOffset, system->screenHeight - mountainsIns.getLocalBounds().height);

			sf::FloatRect spriteRect = mountainsIns.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud1TexIns = new sf::Texture();
			cloud1TexIns->loadFromFile("../assets/SimpleImages/Background_cloud1.png");

			cloud1ins = sf::Sprite();
			cloud1ins.setTexture(*cloud1TexIns);
			cloud1ins.setPosition(xOffset, system->screenHeight - cloud1ins.getLocalBounds().height);

			sf::FloatRect spriteRect = cloud1ins.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud2TexIns = new sf::Texture();
			cloud2TexIns->loadFromFile("../assets/SimpleImages/Background_cloud2.png");

			cloud2ins = sf::Sprite();
			cloud2ins.setTexture(*cloud2TexIns);
			cloud2ins.setPosition(xOffset, system->screenHeight - mountainsIns.getLocalBounds().height + (cloud1ins.getLocalBounds().height / 2));

			sf::FloatRect spriteRect = cloud2ins.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud3TexIns = new sf::Texture();
			cloud3TexIns->loadFromFile("../assets/SimpleImages/Background_cloud3.png");

			cloud3ins = sf::Sprite();
			cloud3ins.setTexture(*cloud3TexIns);
			cloud3ins.setPosition(xOffset, system->screenHeight - mountainsIns.getLocalBounds().height + (cloud1ins.getLocalBounds().height / 2) - cloud2ins.getLocalBounds().height);

			sf::FloatRect spriteRect = cloud3ins.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud4TexIns = new sf::Texture();
			cloud4TexIns->loadFromFile("../assets/SimpleImages/Background_cloud4.png");

			cloud4ins = sf::Sprite();
			cloud4ins.setTexture(*cloud4TexIns);
			cloud4ins.setPosition(xOffset, system->screenHeight - mountainsIns.getLocalBounds().height + (cloud1ins.getLocalBounds().height / 2) - cloud2ins.getLocalBounds().height - (cloud3ins.getLocalBounds().height / 3));

			sf::FloatRect spriteRect = cloud4ins.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud5TexIns = new sf::Texture();
			cloud5TexIns->loadFromFile("../assets/SimpleImages/Background_cloud5.png");

			cloud5ins = sf::Sprite();
			cloud5ins.setTexture(*cloud5TexIns);
			cloud5ins.setPosition(xOffset, system->screenHeight - mountainsIns.getLocalBounds().height + (cloud1ins.getLocalBounds().height / 2) - cloud2ins.getLocalBounds().height - (cloud3ins.getLocalBounds().height / 3) - (cloud4ins.getLocalBounds().height / 2));

			sf::FloatRect spriteRect = cloud5ins.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
	}
	{
		blackTex = new sf::Texture();
		blackTex->loadFromFile("../assets/SimpleImages/Tunnel_black.png");

		blackSpr = sf::Sprite();
		blackSpr.setTexture(*blackTex);

		{
			sf::FloatRect spriteRect = blackSpr.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
			blackSpr.setScale(scaleX, scaleY);
			blackSpr.scale(1.f, -1.f);
		}

		blackSpr.setPosition(xOffset, system->screenHeight);

		overlayTex = new sf::Texture();
		overlayTex->loadFromFile("../assets/SimpleImages/Train_light.png");

		overlaySpr = sf::Sprite();
		overlaySpr.setTexture(*overlayTex);

		{
			sf::FloatRect spriteRect = overlaySpr.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
			overlaySpr.setOrigin(overlaySpr.getLocalBounds().width / 2, overlaySpr.getLocalBounds().height / 2);
			overlaySpr.setScale(scaleX, scaleY);
			overlaySpr.scale(3.f, -1.f);
		}

		overlaySpr.setPosition(xOffset, system->screenHeight / 2);
		overlaySpr.setColor(sf::Color(255, 255, 255, 125));
	}													
}

bool InsideState::Update(float deltatime) {
	sf::Vector2i cursorPosition = sf::Mouse::getPosition(system->window);
	cameraInside->Update();

	//Update All
	{
		/*if (system->player->Getpoweruptype() == PowerUpType_Slowmotion)
		{
			deltatime *= 0.5;
		}
		else if (system->player->Getpoweruptype() == PowerUpType_Invalid) {}*/

		//Update Player
		if (system->player->update(deltatime)) {
//			system->projectileManager->Create(1,
	//			system->player->getRect()->left + system->player->getRect()->width,
		//		system->player->getRect()->top);
		}

		//update player, stress
		system->player->increaseStress(deltatime / 10);

		//update projectiles, also cleaning up
		system->projectileManager->Update(deltatime);

		//update player, check if lose because of high stress
		if (system->player->getStress() >= 10) { system->hasWon = false; return false; }

		//update enemies
		system->enemyManager->Update(deltatime);

		//update Hud
		hud->Update();

		//update owlet
		system->owletManager->Update(deltatime);
		/*for (int i = 0; i < 4; i++) {
			if (!system->player->GetOwlet(i)->IsIdle())
				system->player->GetOwlet(i)->Update(deltatime);
		}*/
	}

	//Check Collisions
	{
		system->projectileManager->checkCollisions();
	}

	//Draw Everything
	{
		system->window.clear(sf::Color(20, 50, 45, 1));
		//Draw World Objects
		{
			cameraInside->Update();

			system->window.draw(starsIns);
			system->window.draw(cloud5ins);
			system->window.draw(cloud4ins);
			system->window.draw(cloud3ins);
			system->window.draw(cloud2ins);
			system->window.draw(cloud1ins);
			system->window.draw(mountainsIns);
			system->window.draw(blackSpr);

			LampParallax->ChangePos(LampParallax->GetX() - 25, LampParallax->GetY());
			LampParallax->Draw(system);
			if (LampParallax->GetX() < xOffset - 650 && !outside) {
				LampParallax->ChangePos(system->screenWidth + xOffset + 300, LampParallax->GetY());
			}
			else if(LampParallax->GetX() < xOffset - 650 && outside){
				LampParallax->ChangePos(blackSpr.getGlobalBounds().left, LampParallax->GetY());
			}

			background->Draw(system);
			background2->Draw(system);

			system->window.draw(*tempSwitch->GetSpriteInside());
			if (system->player->getRect()->intersects(*tempSwitch->GetRect())) { return false; }

			//draw player
			system->player->Draw();

			//draw projectiles
			system->projectileManager->Draw();

			//draw enemies
			system->enemyManager->Draw();

			//draw hardspawned objects
			system->obstacleManager->Draw();

			//Draw Owlets
			system->owletManager->Draw();
			/*for (int i = 0; i < 4; i++) {
				if (!system->player->GetOwlet(i)->IsIdle())
					system->window.draw(*system->player->GetOwlet(i)->getSprite());
			}*/

			for (int i = 0; i < 4; i++) {
				system->window.draw(chairs[i]);
			}

			forground->ChangePos(LampParallax->GetX() - 1725, system->screenHeight / 1.15);
			forground->Draw(system);
			forground2->Draw(system);

			overlaySpr.setPosition(LampParallax->GetX() + 200, system->screenHeight / 2);
			system->window.draw(overlaySpr);

			//Set displayHitbox position and size then draw
			//displayHitbox.setPosition(sf::Vector2f(tempSwitch->GetRect()->left, tempSwitch->GetRect()->top));
			//displayHitbox.setSize(sf::Vector2f(tempSwitch->GetRect()->width, tempSwitch->GetRect()->height));
			//system->window.draw(displayHitbox);
		}

		//Draw Hud Elements
		{
			staticElementsInside->Update();

			//Draw hud
			hud->Draw();
		}

		system->window.display();
	}

	//if (sf::Keyboard::isKeyPressed(sf::Keyboard::O)) { return false; }

	internalTimer += deltatime;
	if (internalTimer >= timerMax) {
		blackSpr.setPosition(blackSpr.getPosition().x - 25, blackSpr.getPosition().y);
		if (alpha > 0) {
			alpha -= deltatime * 150;
			forground->SetColor(sf::Color(0, 0, 0, alpha));
			forground2->SetColor(sf::Color(0, 0, 0, alpha));
			overlaySpr.setColor(sf::Color(0, 0, 0, alpha));
			outside = true;
		}
		else if (count == 0 && alpha <= 0) {
			tempSwitch->SetRect(tempSwitch->GetSprite()->getGlobalBounds().left, tempSwitch->GetSprite()->getGlobalBounds().top + tempSwitch->GetSprite()->getGlobalBounds().height);
			count++;
		}
	}

	return true;
}

void InsideState::Enter() {
	xOffset = system->player->getRect()->left - system->screenWidth / 4;

	tempSwitch = new StateSwitch(200 + xOffset, 0);
	tempSwitch->SetRect(tempSwitch->GetSprite()->getGlobalBounds().left, tempSwitch->GetSprite()->getGlobalBounds().top);
	internalTimer = 0;
	count = 0;
	alpha = 55;
	outside = false;
	forground->SetColor(sf::Color(0, 0, 0, 25));
	forground2->SetColor(sf::Color(0, 0, 0, 25));
	overlaySpr.setColor(sf::Color(0, 0, 0, 200));

	system->player->SetHeight(30, 100);
	system->player->setYPos(0);
	cameraInside->SetCenter(system->player->getRect()->left + system->screenWidth / 4, system->screenHeight / 2);
	background->ChangePos(xOffset, system->screenHeight - 90);
	background2->ChangePos(xOffset, system->screenHeight - 52);
	blackSpr.setPosition(xOffset, blackSpr.getPosition().y);
	//skySprite.setPosition(xOffset, 0);
	forground2->ChangePos(xOffset, forground2->GetY() + 26);

	starsIns.setPosition(xOffset, starsIns.getPosition().y);
	mountainsIns.setPosition(xOffset, mountainsIns.getPosition().y);
	cloud1ins.setPosition(xOffset, cloud1ins.getPosition().y);
	cloud2ins.setPosition(xOffset, cloud2ins.getPosition().y);
	cloud3ins.setPosition(xOffset, cloud3ins.getPosition().y);
	cloud4ins.setPosition(xOffset, cloud4ins.getPosition().y);
	cloud5ins.setPosition(xOffset, cloud5ins.getPosition().y);


	system->player->setXPos(tempSwitch->GetRect()->left - tempSwitch->GetRect()->width);

	system->player->SetMaxSide(background->GetX(), background->GetX() + system->screenWidth - (system->player->getRect()->width * 2));

	system->enemyManager->Create(4, system->screenWidth / 21 + xOffset, system->screenHeight / 2.2);
	system->enemyManager->Create(5, system->screenWidth / 20 + xOffset + system->screenWidth / 5, system->screenHeight / 2.2);
	system->enemyManager->Create(4, system->screenWidth / 20 + xOffset + system->screenWidth / 3.33 + system->screenWidth / 4, system->screenHeight / 2.2);
	system->enemyManager->Create(5, system->screenWidth / 20 + xOffset + system->screenWidth / 4 + system->screenWidth / 4 + system->screenWidth / 4, system->screenHeight / 2.2);
	
	chair = new sf::Texture;
	chair->loadFromFile("../assets/SimpleImages/Train_chair.png");
	chairs[0] = chair1;
	chairs[1] = chair2;
	chairs[2] = chair3;
	chairs[3] = chair4;
	for (int i = 0; i < 4; i++) {
		chairs[i].setTexture(*chair);
		chairs[i].setPosition(25 + xOffset + (355 * i), system->screenHeight / 2.2);
		if (i % 2 == 1) {
			chairs[i].setScale(-1, 1);
			chairs[i].setPosition(chairs[i].getPosition().x + chairs[i].getLocalBounds().width, chairs[i].getPosition().y);
		}
	}

	if (trainInsideAmbient.getStatus() != 3)
	{
		if (trainInsideAmbient.getLoop() == false)
		{
			trainInsideAmbient.openFromFile("../assets/AUDIO/HumansPanicing.wav");
			trainInsideAmbient.setLoop(true);
			trainInsideAmbient.setVolume(80);
			trainInsideAmbient.play();
		}
	}
}
void InsideState::Exit() {
	cout << "Switching State" << endl;
	system->powerupManager->SetPowerUp(4);			//make wind happen
	system->enemyManager->Erase();
	system->obstacleManager->Erase();
	system->window.clear();
	system->projectileManager->Erase();
	trainInsideAmbient.setLoop(false);
	trainInsideAmbient.stop();
}

int InsideState::NextState()
{
	return 0;
}

int InsideState::GetStateType() { return 1; }