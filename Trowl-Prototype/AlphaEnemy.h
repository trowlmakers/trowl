#pragma once

#include <SFML\Graphics.hpp>

#include "baseEnemy.h"

class AlphaEnemy : public baseEnemy
{
public:
	AlphaEnemy();
	AlphaEnemy(float xPos, float yPos);
	~AlphaEnemy();
	void Activate(float xPos, float yPos);
	void Update(float deltaTime);

	void setXPos(float xPos);
	void setYPos(float yPos); 

	void hit(float power);

	void hitLightning();

	void hitBranch();

	bool collides();
	void flee();
	
	sf::Sprite* getSprite();
	sf::FloatRect* getRect();

	void activate();

	int getType();									  
private:
	sf::Texture privateTexture;
	sf::Sprite* sprite;
	sf::FloatRect* rect;
	float speed;
	bool colliding;
	int state;
	float internalTimer;
};

