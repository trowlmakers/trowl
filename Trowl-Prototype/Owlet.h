#pragma once
#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

class Sprite;
class Texture;
class SpriteCreator;
struct System;
class AnimatedSprite;

class Owlet {
public:
	Owlet(std::string source, float posX, float posY, System* inputSystem);
	~Owlet();

	void Update(float deltaTime);

	void ChangeState(int i);

	sf::Sprite* getSprite();
	sf::FloatRect* getRect();

	bool IsIdle();

	void Activate(float xPos, float yPos);

private:
	sf::Sound soundOut;
	sf::SoundBuffer pickupSound;
	sf::SoundBuffer hitSound;

	SpriteCreator* sprCre;
	sf::Sprite* sprite;
	sf::Texture texture;
	sf::FloatRect* rect;
	System* system;

	AnimatedSprite* flyingSprite;

	bool isIdle;
	float currMove, currLean, xPos, yPos;
	int switchDir, currDir, switchLean, inLine;
};