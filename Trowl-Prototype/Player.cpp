#include "Player.h"				
#include "PlayerHead.h"
#include "Engine.h" //Tempor�rt

#include <SFML/Audio.hpp>

#include <SFML\Window\Keyboard.hpp>
#include <SFML\Graphics\Rect.hpp>
#include <iostream>
#include "baseState.h"
#include "Owlet.h"
#include "AnimatedSprite.h"

float topAcc = 0;
float sideAcc = 0;
				 
/*Owlet* owlet;
Owlet* owlet2;
Owlet* owlet3;
Owlet* owlet4;
Owlet* owlets[4];
Owlet* pickedUp[4];*/

Player::Player() { std::cout << "Wrong constructor used when making Player" << std::endl; }

Player::Player(float xPos, float yPos, System* inputSystem)
{
	system = inputSystem;

	pos.x = xPos;
	pos.y = yPos;

	head = new PlayerHead();

	privateTexture.loadFromFile("../assets/OwlBodyRight.png");
	sprite = new sf::Sprite();
	sprite->setTexture(privateTexture);
	sprite->setPosition(xPos, yPos);

	idleSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Owl_body_wings_sprsheet.png",
		"../assets/AnimatedObjects/Owl_body_wings_sprsheet.txt");
	idleSuperSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Owl_goldenfeather_sprsheet.png",
		"../assets/AnimatedObjects/Owl_goldenfeather_sprsheet.txt");
	lightningedSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Owl_Electrocuted.png",
		"../assets/AnimatedObjects/Owl_Electrocuted.txt");
	physHitSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Owl_Damage_spr-sheet.png",
		"../assets/AnimatedObjects/Owl_Damage_spr-sheet.txt");

	sprite = idleSprite->getSprite();

	rect = new sf::FloatRect(
		xPos,
		yPos + 100,
		sprite->getLocalBounds().width,
		25);

	electrocutionSound.loadFromFile("../assets/AUDIO/Electrocution.wav");
	physHitSound.loadFromFile("../assets/AUDIO/branchDamage.wav");
	shootSound.loadFromFile("../assets/AUDIO/OwlShoot2.wav");
	tunnelHitSound.loadFromFile("../assets/AUDIO/TunnelOwlHit.wav");
	

	/*owlet = new Owlet("../assets/Owlet.png", 1500.0f, system->screenHeight - 115.0f, system);
	owlet2 = new Owlet("../assets/Owlet.png", 4600, system->screenHeight - 115, system);
	owlet3 = new Owlet("../assets/Owlet.png", 9000, system->screenHeight - 115, system);
	owlet4 = new Owlet("../assets/Owlet.png", 12000, system->screenHeight - 115, system);

	owlets[0] = owlet;
	owlets[1] = owlet2;
	owlets[2] = owlet3;
	owlets[3] = owlet4;	*/

	facingRight = true;

	heartBeat.openFromFile("../assets/AUDIO/heart.wav");
	heartBeat.setLoop(true);
	heartBeat.setVolume(0);
	heartBeat.play();

	stressLevel = 0.0f;
				  
	coolDownCounter = 0;
	coolDownTime = 0.5f;

	movementState = 1;
	shootingState = 0;

	xVel = 0;
	yVel = 0;
	speed = 55;		  // Need to increase speed if we increase inertia
	inertia = 0.90;	 // 1 for complete freedom of movement, 0 for locked in place
}					// Lower inertia value for more sticky movement, less for floaty movement
	   
Player::~Player()
{
	heartBeat.setVolume(0);
}

bool Player::update(float deltaTime) {
	head->Update(deltaTime);

	heartBeat.setVolume(stressLevel * 10);

	//movement
	switch (movementState)
	{
	case 1: //standard movement
	{		  
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			yVel -= deltaTime * speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			xVel -= deltaTime / 2 * speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			yVel += deltaTime * speed;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			xVel += deltaTime * speed;
		}
			
		yVel *= inertia; //diminishing returns speed
		xVel *= inertia; // also that

		//if its windy, push player backwards
		if (system->powerupManager->CheckPowerUp() == 4)
		{
			xVel -= deltaTime * 20;	
		}

		pos.x += xVel;
		pos.y += yVel;

		// if player outside bounds, push inside bounds
		{
			if (pos.y < maxHeight)
			{
				pos.y = maxHeight;
			}
			if (pos.y > minHeight + sprite->getLocalBounds().height)
			{
				pos.y = minHeight + sprite->getLocalBounds().height;
			}
			if (pos.x < minSide)
			{
				pos.x = minSide;
			}
			if (pos.x > maxSide + sprite->getLocalBounds().width)
			{
				pos.x = maxSide + sprite->getLocalBounds().width;
			}
		}

		if (system->powerupManager->CheckPowerUp() != 3)
		{									 
			idleSprite->update(deltaTime);
			sprite = idleSprite->getSprite();
		}
		else
		{
			idleSuperSprite->update(deltaTime);
			sprite = idleSuperSprite->getSprite();
		}
	}	   	
		break;
	
	case 2: //electricity hitstun
		movementTimer -= deltaTime;
		if (movementTimer <= 0)
		{
			movementState = 1;
			shootingState = 0;
		}

		lightningedSprite->update(deltaTime);
		sprite = lightningedSprite->getSprite();

		break;
	case 3:	//phys hitstun
		movementTimer -= deltaTime;
		if (movementTimer <= 0)
		{
			movementState = 1;
			shootingState = 0;
			physHitSprite->reset();
		}
		physHitSprite->update(deltaTime);
		sprite = physHitSprite->getSprite();
		pos.x -= movementTimer;	 
		break;
	}
				 
	//shooting
	switch (shootingState) 
	{
	case 0: //not shooting at all
		if (coolDownCounter > 0) { coolDownCounter -= deltaTime; }
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			shootingState = 1;
			shootingTimer = 0;
			if (system->powerupManager->CheckPowerUp() == 1) { shootingTimer += 1; }
		}
		break;
	case 1: //keep charging, or shoot if button is released
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			//std::cout << "charging ma lazor" << std::endl;
			if (shootingTimer < 2) { shootingTimer += deltaTime * 1.8; }
			head->setRed(shootingTimer * 125);
		}
		else	 //if button is released, shoot.
		{
			if (shootingTimer < 0.2) { shootingTimer = 0.2; }
			head->setRed(0);
 			
			system->projectileManager->Create(1, rect->left, rect->top, shootingTimer);
			
			shootingState = 0;
			coolDownCounter = 0.2;
			
			soundOut.setBuffer(shootSound);
			soundOut.setPitch(1 + (rand() % 10) / 9 + shootingTimer / 2);
			soundOut.play();
			
			//head->Scream();
		}
			break;
	}  

	return false;
}

void Player::hitByThunder()
{
	if (system->powerupManager->CheckPowerUp() == 3) {}
	else {		  
		if (movementState != 2)
		{
			system->player->increaseStress(3);
			movementState = 2;
			movementTimer = 1;
			shootingState = 0;
			soundOut.setBuffer(electrocutionSound);
			soundOut.play();
		}
	}
}

void Player::hitByPhysical()
{
	//if (movementState != 3)
	if (system->powerupManager->CheckPowerUp() == 3) {}
	else if (movementState != 3)
	{				  
		system->player->increaseStress(2);
		movementState = 3;
		movementTimer = 1;
		shootingState = 0;
		soundOut.setBuffer(physHitSound);
		soundOut.play();   
	}
}
void Player::hitByTunnel()
{	  
	system->player->increaseStress(100);
	movementState = 3;
	movementTimer = 1;
	shootingState = 0;
	soundOut.setBuffer(tunnelHitSound);
	soundOut.play();
}

void Player::Draw()
{							
	if (movementState == 1)	
	{
		sprite->setPosition(pos.x, pos.y);
		system->window.draw(*sprite);

		head->setHeadPosition(
			pos.x + rect->width - head->getSprite()->getLocalBounds().width,
			pos.y + 89);

		if (system->powerupManager->CheckPowerUp() == 3) { head->setGolden(true); }
		else { head->setGolden(false); }
		system->window.draw(*head->getSprite());

		sf::RectangleShape* hitboxOut = new sf::RectangleShape;
		hitboxOut->setPosition(rect->left, rect->top);
		hitboxOut->setSize(sf::Vector2f(rect->width, rect->height));
		hitboxOut->setFillColor(sf::Color(0, 130, 0, 100));
		//system->window.draw(*hitboxOut);
	}

	else
	{
		sprite->setPosition(pos.x, pos.y);
		system->window.draw(*sprite);
	}
}
 
void Player::setXPos(float xPos) 
{
	pos.x = xPos;
}

void Player::setYPos(float yPos) 
{
	pos.y = yPos;
}

void Player::increaseStress(float increase) 
{
	stressLevel += increase;
}

void Player::decreaseStress(float decrease) 
{
	stressLevel -= decrease;
	if (stressLevel < 0 || decrease >= 99) { stressLevel = 0; }
}

float Player::getStress() 
{
	return stressLevel;
}
							   
bool Player::IsFacingRight()
{
	return facingRight;
} 

sf::FloatRect* Player::getRect() 
{
	rect->left = pos.x;
	rect->top = pos.y + 90;
	return rect; 
}

float Player::GetAngle() 
{
	return angle;
}

void Player::SetHeight(float x, float y) {
	maxHeight = x;
	minHeight = y;
}

void Player::SetMaxSide(float min, float max) {
	minSide = min;
	maxSide = max;
}

float Player::GetMinSide() {
	if (rect->left > minSide + system->screenWidth / 4)
		minSide = pos.x - system->screenWidth / 4;
	return minSide;
}

float Player::GetSpeed() {
	return xVel;
}