#pragma once

#include <SFML\Graphics.hpp>

struct System;

class FrozenScreen
{
public:
	FrozenScreen();
	~FrozenScreen();
	void Initialize(System * systemIn);
	void Freeze(std::string file);

private:
	sf::Texture internalTexture;
	sf::Sprite spriteOut;
	System* system;
};

								   