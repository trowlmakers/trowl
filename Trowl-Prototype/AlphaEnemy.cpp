#include "AlphaEnemy.h"

#include <SFML\Graphics.hpp>
#include <iostream>

AlphaEnemy::AlphaEnemy() {}

AlphaEnemy::AlphaEnemy(float xPos, float yPos){
	speed = 100;

	privateTexture.loadFromFile("../assets/SimpleImagesFalcon.png");
	sprite = new sf::Sprite();
	sprite->setTexture(privateTexture);
	sprite->setPosition(0, 0);

	rect = new sf::FloatRect(xPos, yPos, sprite->getLocalBounds().width, sprite->getLocalBounds().height);

	state = 0;
	internalTimer = 0;
	colliding = true;
}

AlphaEnemy::~AlphaEnemy()
{
}


void AlphaEnemy::Activate(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
	state = 0;
	colliding = true;
	speed = 200;
}

void AlphaEnemy::Update(float deltaTime) {
	switch (state)
	{
	case 0:					   //normal
		rect->left -= speed* deltaTime;
		break;
	case 1:					   //fleeing
		rect->left -= speed* deltaTime;
		rect->top -= speed* deltaTime;
		break;
	case 2:					   //hitstun
		std::cout << "currently in hitstun" << std::endl;
		internalTimer -= deltaTime;
		if (internalTimer < 0) { state = 1; }
		break;
	}
}

void AlphaEnemy::setXPos(float xPos) { rect->left = xPos; }
void AlphaEnemy::setYPos(float yPos) { rect->top = yPos; }

void AlphaEnemy::hit(float power)
{
	internalTimer = 0.3;
	state = 2;
	speed *= 1.5;
	colliding = false;
}

void AlphaEnemy::hitLightning() {}
void AlphaEnemy::hitBranch() {}

bool AlphaEnemy::collides()
{
	return colliding;
}

void AlphaEnemy::flee()
{
}



sf::Sprite* AlphaEnemy::getSprite() {
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

sf::FloatRect* AlphaEnemy::getRect() { return rect; }

void AlphaEnemy::activate()
{
	state = 0;
	colliding = true;
}

int AlphaEnemy::getType() { return 1; }