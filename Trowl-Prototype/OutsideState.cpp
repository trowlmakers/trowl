#include "OutsideState.h"
#include "Engine.h"
#include "SpriteCreator.h"
#include "Player.h"
#include "StateSwitch.h"
#include "Owlet.h"
#include "Hud.h"
#include "TileCreator.h"

#include "Human.h"

#include "Camera.h"
#include "AnimatedThing.h"	

#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include <cctype>
#include <string>
#include <sstream>	  

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#pragma comment(lib, "sfml-audio.lib")
#endif

using namespace std;
using namespace sf;

sf::Font* textFont = new sf::Font();

TileCreator* cart1;
TileCreator* cart2;
TileCreator* cart3;
TileCreator* cart4;
TileCreator* carts[4];

sf::Texture trainTex;
sf::Sprite* train;

TileCreator* TreeParallax1;
TileCreator* TreeParallax2;
TileCreator* TreeParallax3;
TileCreator* TreeParallax4;
TileCreator* TreeParallax5;
TileCreator* backgroundTrees[5];

sf::Sprite stars;
sf::Sprite stars2;
sf::Texture* starsTex;
sf::Sprite cloud1;
sf::Sprite cloud1_2;
sf::Texture* cloud1Tex;
sf::Sprite cloud2;
sf::Sprite cloud2_2;
sf::Texture* cloud2Tex;
sf::Sprite cloud3;
sf::Sprite cloud3_2;
sf::Texture* cloud3Tex;
sf::Sprite cloud4;
sf::Sprite cloud4_2;
sf::Texture* cloud4Tex;
sf::Sprite cloud5;
sf::Sprite cloud5_2;
sf::Texture* cloud5Tex;
sf::Sprite mountains;
sf::Sprite mountains2;
sf::Texture* mountainsTex;
vector<sf::Sprite*> background;
float bgSpeed = 0.25;

sf::RectangleShape hitBoxTest; //Note that this doesn't really do anything else then display the hitbox rect

//Two pointers for two cameras, one displaying the game, the other displaying the hud
Camera* camera;
Camera* staticElements;

sf::Vector2i loadCounter = sf::Vector2i(0, 0);

OutsideState::OutsideState(){}	  
OutsideState::~OutsideState() {
	delete cart1;
	cart1 = nullptr;
	delete cart2;
	cart2 = nullptr;
	delete cart3;
	cart3 = nullptr;
	delete cart4;
	cart4 = nullptr;
	
	delete train;
	train = nullptr;

	delete TreeParallax1;
	TreeParallax1 = nullptr;
	delete TreeParallax2;
	TreeParallax2 = nullptr;
	delete TreeParallax3;
	TreeParallax3 = nullptr;
	delete TreeParallax4;
	TreeParallax4 = nullptr;
	delete TreeParallax5;
	TreeParallax5 = nullptr;

	delete starsTex;
	starsTex = nullptr;
	delete cloud1Tex;
	cloud1Tex = nullptr;
	delete cloud2Tex;
	cloud2Tex = nullptr;
	delete cloud3Tex;
	cloud3Tex = nullptr;
	delete cloud4Tex;
	cloud4Tex = nullptr;
	delete cloud5Tex;
	cloud5Tex = nullptr;
	delete mountainsTex;
	mountainsTex = nullptr;
	background.clear();

	delete camera;
	camera = nullptr;
	delete staticElements;
	staticElements = nullptr;

	system = nullptr;

	delete hud;
	hud = nullptr;
}
	
void OutsideState::Initiate(System* inputSystem) {
	system = inputSystem;

	//Set the looks of the Hitbox Test
	hitBoxTest.setFillColor(sf::Color(0, 255, 0, 125));
	hitBoxTest.setOutlineColor(sf::Color(255, 0, 0));
	hitBoxTest.setOutlineThickness(1);

	system->powerupManager->Create(3, 1000, 400);

	trainTex.loadFromFile("../assets/SimpleImages/Lokomotive_spr.png");
	train = new sf::Sprite;
	train->setTexture(trainTex);

	//Creating both cameras
	camera = new Camera(system, system->player); //Include player as the target for the camera to follow
	staticElements = new Camera(system, 0, 0); //Don't include player since this camera is suppose to be static and only display static elements

	//Create Background
	{
		//Create Sprites w/ textures
		{
			starsTex = new sf::Texture();
			starsTex->loadFromFile("../assets/SimpleImages/Background_sky.png");

			stars = sf::Sprite();
			stars.setTexture(*starsTex);
			stars.setPosition(0, 0);

			sf::FloatRect spriteRect = stars.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			mountainsTex = new sf::Texture();
			mountainsTex->loadFromFile("../assets/SimpleImages/Background_mountains.png");

			mountains = sf::Sprite();
			mountains.setTexture(*mountainsTex);
			mountains.setPosition(0, system->screenHeight - mountains.getLocalBounds().height);

			sf::FloatRect spriteRect = mountains.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud1Tex = new sf::Texture();
			cloud1Tex->loadFromFile("../assets/SimpleImages/Background_cloud1.png");

			cloud1 = sf::Sprite();
			cloud1.setTexture(*cloud1Tex);
			cloud1.setPosition(0, system->screenHeight - cloud1.getLocalBounds().height);

			sf::FloatRect spriteRect = cloud1.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud2Tex = new sf::Texture();
			cloud2Tex->loadFromFile("../assets/SimpleImages/Background_cloud2.png");

			cloud2 = sf::Sprite();
			cloud2.setTexture(*cloud2Tex);
			cloud2.setPosition(0, system->screenHeight - mountains.getLocalBounds().height + (cloud1.getLocalBounds().height / 2));

			sf::FloatRect spriteRect = cloud2.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud3Tex = new sf::Texture();
			cloud3Tex->loadFromFile("../assets/SimpleImages/Background_cloud3.png");

			cloud3 = sf::Sprite();
			cloud3.setTexture(*cloud3Tex);
			cloud3.setPosition(0, system->screenHeight - mountains.getLocalBounds().height + (cloud1.getLocalBounds().height / 2) - cloud2.getLocalBounds().height);

			sf::FloatRect spriteRect = cloud3.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud4Tex = new sf::Texture();
			cloud4Tex->loadFromFile("../assets/SimpleImages/Background_cloud4.png");

			cloud4 = sf::Sprite();
			cloud4.setTexture(*cloud4Tex);
			cloud4.setPosition(0, system->screenHeight - mountains.getLocalBounds().height + (cloud1.getLocalBounds().height / 2) - cloud2.getLocalBounds().height - (cloud3.getLocalBounds().height / 3));

			sf::FloatRect spriteRect = cloud4.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		{
			cloud5Tex = new sf::Texture();
			cloud5Tex->loadFromFile("../assets/SimpleImages/Background_cloud5.png");

			cloud5 = sf::Sprite();
			cloud5.setTexture(*cloud5Tex);
			cloud5.setPosition(0, system->screenHeight - mountains.getLocalBounds().height + (cloud1.getLocalBounds().height / 2) - cloud2.getLocalBounds().height - (cloud3.getLocalBounds().height / 3) - (cloud4.getLocalBounds().height / 2));

			sf::FloatRect spriteRect = cloud5.getLocalBounds();
			float scaleX = system->screenWidth / spriteRect.width;
			float scaleY = system->screenHeight / spriteRect.height;
		}
		//Create copies of the created ones
		{
			stars2 = stars;
			cloud1_2 = cloud1;
			cloud2_2 = cloud2;
			cloud3_2 = cloud3;
			cloud4_2 = cloud4;
			cloud5_2 = cloud5;
			mountains2 = mountains;
		}
		//Add all sprites to vector
		{
			background.push_back(&stars);
			background.push_back(&stars2);
			background.push_back(&cloud5);
			background.push_back(&cloud5_2);
			background.push_back(&cloud4);
			background.push_back(&cloud4_2);
			background.push_back(&cloud3);
			background.push_back(&cloud3_2);
			background.push_back(&cloud2);
			background.push_back(&cloud2_2);
			background.push_back(&cloud1);
			background.push_back(&cloud1_2);
			background.push_back(&mountains);
			background.push_back(&mountains2);
		}
	}							

	//init testing objects
	{			
		sf::Texture texture;

		hud = new Hud();
		hud->Initialize(system);

		cart1 = new TileCreator(250, 120, 30, 30, "../assets/Tilesets/trainWagonTileMap.txt");
		carts[0] = cart1;
		cart2 = new TileCreator(1000, 120, 30, 30, "../assets/Tilesets/trainWagonTileMap.txt");
		carts[1] = cart2;
		cart3 = new TileCreator(1750, 120, 30, 30, "../assets/Tilesets/trainWagonTileMap.txt");
		carts[2] = cart3;
		cart4 = new TileCreator(2500, 120, 30, 30, "../assets/Tilesets/trainWagonTileMap.txt");
		carts[3] = cart4;

		TreeParallax1 = new TileCreator(-1000, 300, 30, 30, "../assets/Tilesets/treeTileMap_1.txt");
		backgroundTrees[0] = TreeParallax1;
		TreeParallax2 = new TileCreator(-100, 275, 30, 30, "../assets/Tilesets/treeTileMap_2.txt");
		backgroundTrees[1] = TreeParallax2;
		TreeParallax3 = new TileCreator(-1000, 220, 30, 30, "../assets/Tilesets/treeTileMap_3.txt");
		backgroundTrees[2] = TreeParallax3;
		TreeParallax4 = new TileCreator(-1000, 285, 30, 30, "../assets/Tilesets/treeTileMap_4.txt");
		backgroundTrees[3] = TreeParallax4;
		TreeParallax5 = new TileCreator(-1000, 250, 30, 30, "../assets/Tilesets/treeTileMap_2.txt");
		backgroundTrees[4] = TreeParallax5;
	}
}

//return false if we want to change state
bool OutsideState::Update(float deltatime) {
	sf::Vector2i cursorPosition = sf::Mouse::getPosition(system->window);

	//update everything
	{						   		
		if (system->powerupManager->CheckPowerUp() == 2)
		{
			deltatime *= 0.5;
		} 

		//update player, shooting
		camera->Update();
		//system->player->SetMaxSide(system->player->GetMinSide(), system->player->getRect()->left + system->screenWidth);

		//update player, stress
		system->player->increaseStress(deltatime/10);

		system->player->update(deltatime);
		system->projectileManager->Update(deltatime);
		system->enemyManager->Update(deltatime);
		system->powerupManager->Update(deltatime);
		system->particleManager->Update(deltatime);
		system->spawnChecker->Update();

		//update obstacles, if false we have collided with stateswitch  
		if (system->obstacleManager->Update(deltatime) == false) { return false; }	

		staticElements->Update();

		//update player, check if lose because of high stress
		if (system->player->getStress() >= 10) { system->hasWon = false; return false; }

		//check if end of game.
		if (system->player->getRect()->left > 24000) { 
			system->hasWon = true; 
			return false; 
		}

		//update owlet
		system->owletManager->Update(deltatime);

		//update Hud
		hud->Update();

		//Update Backgroud
		{
			stars.setPosition(stars.getPosition().x - (1 * bgSpeed), stars.getPosition().y);
			stars2.setPosition(stars2.getPosition().x - (1 * bgSpeed), stars2.getPosition().y);

			cloud1.setPosition(cloud1.getPosition().x - (6 * bgSpeed), cloud1.getPosition().y);
			cloud1_2.setPosition(cloud1_2.getPosition().x - (6 * bgSpeed), cloud1_2.getPosition().y);

			cloud2.setPosition(cloud2.getPosition().x - (5 * bgSpeed), cloud2.getPosition().y);
			cloud2_2.setPosition(cloud2_2.getPosition().x - (5 * bgSpeed), cloud2_2.getPosition().y);

			cloud3.setPosition(cloud3.getPosition().x - (4 * bgSpeed), cloud3.getPosition().y);
			cloud3_2.setPosition(cloud3_2.getPosition().x - (4 * bgSpeed), cloud3_2.getPosition().y);

			cloud4.setPosition(cloud4.getPosition().x - (3 * bgSpeed), cloud4.getPosition().y);
			cloud4_2.setPosition(cloud4_2.getPosition().x - (3 * bgSpeed), cloud4_2.getPosition().y);

			cloud5.setPosition(cloud5.getPosition().x - (2 * bgSpeed), cloud5.getPosition().y);
			cloud5_2.setPosition(cloud5_2.getPosition().x - (2 * bgSpeed), cloud5_2.getPosition().y);

			mountains.setPosition(mountains.getPosition().x - (7 * bgSpeed), mountains.getPosition().y);
			mountains2.setPosition(mountains2.getPosition().x - (7 * bgSpeed), mountains2.getPosition().y);
		}
	}		 

	//check all collisions
	{								 
		//check collision: projectiles - Enemies
		system->projectileManager->checkCollisions();
	}

	//draw everything
	{
		//DRAW WORLD OBJECTS

		//clear screen
		system->window.clear(Color(200, 60, 40, 1));

		//draw background
		for (int i = 0; i < background.size(); i++) {
			sf::View currView = system->window.getView();
			sf::FloatRect viewRect(currView.getCenter() - currView.getSize() / 2.f, currView.getSize());

			if (!viewRect.contains(background[i]->getPosition().x + background[i]->getLocalBounds().width, background[i]->getPosition().y) && background[i]->getPosition().x < viewRect.left) {
				background[i]->setPosition(background[i]->getPosition().x + (background[i]->getLocalBounds().width * 2), background[i]->getPosition().y);
			}

			system->window.draw(*background[i]);
		}

		//Update camera (the one that displays the game world)
		camera->Update();

		//Draw and Update Trees
		for (int i = 0; i < 5; i++) 
		{
			backgroundTrees[i]->ChangePos(backgroundTrees[i]->GetX() - (5 + rand() % (10 - 5) + 5), backgroundTrees[i]->GetY());
			backgroundTrees[i]->Draw(system);

			if (system->player->getRect()->left - backgroundTrees[i]->GetX() > system->screenWidth + 250) {
				int j = rand() % (1000, 2000) + 1000;
				backgroundTrees[i]->ChangePos(system->player->getRect()->left + j, backgroundTrees[i]->GetY());
			}
		}

		system->obstacleManager->DrawTrees();

		//draw Train
		for (int i = 0; i < 4; i++)
		{
			if (carts[i]->GetX() + 750 < system->player->GetMinSide()) 
			{
				carts[i]->ChangePos(carts[i]->GetX() + 2250, carts[i]->GetY());
			}
			if(carts[i]->GetX() < 24000)
				carts[i]->Draw(system);
			else {
				train->setPosition(carts[i]->GetX(), carts[i]->GetY() * 4);
				system->window.draw(*train);
			}
		}

		system->projectileManager->Draw();
		system->powerupManager->Draw();
		system->enemyManager->Draw();
		system->particleManager->Draw();
		system->obstacleManager->DrawWarning();
		system->obstacleManager->Draw();
		system->player->Draw();
		hud->Draw();

		//draw owlets
		system->owletManager->Draw();
	//	system->owletManager->DrawHitBoxes();

		//DRAW STATIC OBJECT (AKA HUD AND SIMULAR)
		//Updating Static Camera (Camera that display Hud Elements)
		staticElements->Update();

		//Draw Warning
	//	system->obstacleManager->DrawWarning();


		//draw score
		sf::Text tempText = sf::Text();
		tempText.setFont(*textFont);
		tempText.setCharacterSize(30);
		tempText.setColor(sf::Color::Black);
		tempText.setPosition(sf::Vector2f(system->screenWidth - 50,  0));

		std::string Result;          // string which will contain the result  
		std::ostringstream convert;   // stream used for the conversion		  
		convert << system->score;      // insert the textual representation of 'Number' in the characters in the stream
		Result = convert.str(); // set 'Result' to the contents of the stream
		tempText.setString(Result);

		system->window.draw(tempText);

		system->window.display();
	}

	return true;																				
}

void OutsideState::Enter()
{

	system->player->SetHeight(-90, 280);
	system->player->setYPos(280);

	if (bgmMusic.getStatus() != 3)
	{
		if (bgmMusic.getLoop() == false)
		{
			bgmMusic.openFromFile("../assets/AUDIO/BackgroundMusic.wav");
			bgmMusic.setLoop(true);
			bgmMusic.setVolume(60);
			bgmMusic.play();
		}
	}	

	if (trainAmbient.getStatus() != 3)
	{
		if (trainAmbient.getLoop() == false)
		{
			trainAmbient.openFromFile("../assets/AUDIO/TrainAmbient.wav");
			trainAmbient.setLoop(true);
			trainAmbient.play();
		}
	}
	trainAmbient.setVolume(25);

	stars.setPosition(0, stars.getPosition().y);
	cloud1.setPosition(0, cloud1.getPosition().y);
	cloud2.setPosition(0, cloud2.getPosition().y);
	cloud3.setPosition(0, cloud3.getPosition().y);
	cloud4.setPosition(0, cloud4.getPosition().y);
	cloud5.setPosition(0, cloud5.getPosition().y);
	mountains.setPosition(0, mountains.getPosition().y);

	stars2.setPosition(stars.getPosition().x + stars.getLocalBounds().width, stars2.getPosition().y);
	cloud1_2.setPosition(cloud1.getPosition().x + cloud1.getLocalBounds().width, cloud1_2.getPosition().y);
	cloud2_2.setPosition(cloud2.getPosition().x + cloud2.getLocalBounds().width, cloud2_2.getPosition().y);
	cloud3_2.setPosition(cloud3.getPosition().x + cloud3.getLocalBounds().width, cloud3_2.getPosition().y);
	cloud4_2.setPosition(cloud4.getPosition().x + cloud4.getLocalBounds().width, cloud4_2.getPosition().y);
	cloud5_2.setPosition(cloud5.getPosition().x + cloud5.getLocalBounds().width, cloud5_2.getPosition().y);
	mountains2.setPosition(mountains.getPosition().x + mountains.getLocalBounds().width, mountains2.getPosition().y);
}

void OutsideState::Exit() 
{
	cout << "Switching State" << endl;
	system->enemyManager->Erase();
	system->obstacleManager->Erase();
	system->window.clear();
	//trainAmbient.stop();
	trainAmbient.setVolume(15);
}

int OutsideState::NextState() 
{
	system->window.clear();

	if (system->player->getStress() >= 10) { bgmMusic.stop(); return 2; }		 //goto endstate
	else if (system->player->getRect()->left > 15000) { bgmMusic.stop(); return 2; }
	else if (Keyboard::isKeyPressed(Keyboard::Escape)) { bgmMusic.stop(); return 3; }			//goto manustate
	else { return 1; }			//goto insidestate

}

//0 = outsideState, 1 = insideState, 2 = loseState
int OutsideState::GetStateType() { return 0; }
