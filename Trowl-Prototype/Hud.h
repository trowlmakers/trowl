#pragma once

#include <SFML\Graphics.hpp>

struct System;

class Hud
{
public:
	Hud();
	void Initialize(System * systemIn);
	~Hud();

	void Hud::Update();
	void Hud::Draw();
private:
	sf::Texture stressContainerTexture;
	sf::Texture stressFillerTexture;

	sf::Sprite stressContainerSprite;
	sf::Sprite stressFillerSprite;

	System* system;		

	float currentStress;		

	sf::Text textOut;
	sf::Font textFont;

	float containerSize;
};

