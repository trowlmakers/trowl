#pragma once

#include "baseState.h"

class PlayerProjectile;
class Player;
class Sprite;
class Texture;
class StateSwitch;

class InsideState : public baseState
{
public:
	InsideState();
	~InsideState();
	bool Update(float deltaTime);
	void Initiate(System* inputSystem);
	void Enter();
	void Exit();
	int NextState();
	int GetStateType();
private:
	System* system;	

	sf::Sprite blackSpr;
	sf::Texture* blackTex;
	sf::Sprite overlaySpr;
	sf::Texture* overlayTex;

	sf::Texture* chair;
	sf::Sprite chair1;
	sf::Sprite chair2;
	sf::Sprite chair3;
	sf::Sprite chair4;
	sf::Sprite chairs[4];

	StateSwitch* tempSwitch;

	sf::Music trainInsideAmbient;

	float xOffset = 0;
	float internalTimer = 0, timerMax = 20, alpha = 55;
	bool outside = false;
	int count = 0;

};

