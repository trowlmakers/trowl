#include <iostream>
#include <SFML\Graphics.hpp>
#include <math.h>

#include "baseState.h"
#include "HumanProjectile.h"
#include "baseProjectile.h"


HumanProjectile::HumanProjectile() { std::cout << "HumanProjectile spawned using wrong constructor" << std::endl; }
HumanProjectile::~HumanProjectile() {}


HumanProjectile::HumanProjectile(float xPos, float yPos, float angle)
{
	sprite = new sf::Sprite();

	int obj = rand() % 6 + 1;
	switch (obj) {
	case 1:
		privateTexture.loadFromFile("../assets/SimpleImages/Thrown_Obj_Banana3.png");
		sprite->setScale(1.5, 1.5);
		break;
	case 2:
		privateTexture.loadFromFile("../assets/SimpleImages/Thrown_Obj_Flaska3.png");
		sprite->setScale(2, 2);
		break;
	case 3:
		privateTexture.loadFromFile("../assets/SimpleImages/Thrown_Obj_Keps3.png");
		sprite->setScale(2, 2);
		break;
	case 4:
		privateTexture.loadFromFile("../assets/SimpleImages/Thrown_Obj_Laptop3.png");
		sprite->setScale(3, 3);
		break;
	case 5:
		privateTexture.loadFromFile("../assets/SimpleImages/Thrown_Obj_Macka3.png");
		sprite->setScale(2, 2);
		break;
	case 6:
		privateTexture.loadFromFile("../assets/SimpleImages/Thrown_Obj_Tennisball3.png");
		sprite->setScale(1, 1);
		break;
	}
	sprite->setTexture(privateTexture);
	sprite->setPosition(0, 0);
	sprite->setOrigin(sprite->getLocalBounds().width / 2, sprite->getLocalBounds().height / 2);


	rect = new sf::FloatRect(xPos, yPos,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	float baseSpeed = 600;

	xVel = baseSpeed * cos(angle);
	yVel = baseSpeed * sin(angle);
	weight = 10;
}

void HumanProjectile::activate(float xPos, float yPos) {
	rect->left = xPos;
	rect->top = yPos;		
	
	float baseSpeed = 600;

	float angle = 50;
	xVel = baseSpeed * cos(angle);
	yVel = baseSpeed * sin(angle);
}

void HumanProjectile::Update(float deltatime) {
	yVel += weight;
	rect->left += (deltatime * xVel);
	rect->top += (deltatime * yVel);
	sprite->rotate(deltatime * 500);
}


sf::FloatRect* HumanProjectile::getRect() { return rect; }

sf::Sprite* HumanProjectile::getSprite() {
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

int HumanProjectile::getType() { return 2; }

void HumanProjectile::setPower(float powerIn)
{
	power = powerIn;
}

float HumanProjectile::getPower()
{
	return power;
}

