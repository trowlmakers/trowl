#include "SpawnChecker.h"

#include <vector>

#include <iostream>
#include <fstream>

#include "baseState.h"
#include "Player.h"

std::vector<SpawnNode*> activeNodes;
	
SpawnChecker::SpawnChecker() {}
SpawnChecker::~SpawnChecker() {
	activeNodes.clear();
}

void SpawnChecker::Initialize(System * systemIn)
{
	system = systemIn;

	std::ifstream infile("../assets/Level.txt");
	
	while (infile.eof() != true)
	{
		SpawnNode* newnode = new SpawnNode;
		infile >> newnode->spawnPosition;
		infile >> newnode->spawnCategory;
		infile >> newnode->spawnType;
		infile >> newnode->spawnXpos;
		infile >> newnode->spawnYpos;
		activeNodes.push_back(newnode);
	}
	infile.close();
}

void SpawnChecker::Update()
{
	if (activeNodes.size() > 0) {
		if (system->player->getRect()->left > activeNodes[0]->spawnPosition)
		{
			//std::cout << "Spawn triggered" << std::endl;
			switch (activeNodes[0]->spawnCategory)
			{
			case 1:
				system->enemyManager->Create(
					activeNodes[0]->spawnType,
					activeNodes[0]->spawnXpos,
					activeNodes[0]->spawnYpos);
				break;
			case 2:
				system->obstacleManager->Create(
					activeNodes[0]->spawnType,
					activeNodes[0]->spawnXpos,
					activeNodes[0]->spawnYpos);
				break;
			case 3:
				system->powerupManager->Create(
					activeNodes[0]->spawnType,
					activeNodes[0]->spawnXpos,
					activeNodes[0]->spawnYpos);
				break;
			case 4:
				system->owletManager->Create(
					activeNodes[0]->spawnXpos);
				break;
			}
			activeNodes.erase(activeNodes.begin());
			this->Update();
		}
	}
}
																    