#pragma once
#include <vector>
#include <SFML\Graphics.hpp>


class basePowerUp
{
public:
	virtual ~basePowerUp() {};
	virtual void Update(float deltaTime) = 0;

	virtual sf::FloatRect* getRect() = 0;
	virtual sf::Sprite* getSprite() = 0;

	virtual int getType() = 0;
	//virtual void FadeOut() = 0;
};

