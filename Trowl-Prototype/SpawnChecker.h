#pragma once

struct System;


struct SpawnNode
{
	int spawnPosition;
	int spawnCategory; //1 for enemy, 2 for trap, 3 for powerup
	int spawnType;
	int spawnXpos;
	int spawnYpos;
};


class SpawnChecker
{
public:
	SpawnChecker();
	~SpawnChecker();
	void Initialize(System* systemIn);
	void Update();
private:
	System* system;

};

