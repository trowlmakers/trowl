#include "Feather.h"

#include "baseParticle.h"

#include <SFML\Graphics.hpp>
#include <SFML\System.hpp  >
#include <iostream>

Feather::~Feather() {}
Feather::Feather()
{
	int random = rand() % 3 + 1;
	switch (random) {
	case 1:
		privateTexture.loadFromFile("../assets/SimpleImages/Falcon_Feather.png");
		break;
	case 2:
		privateTexture.loadFromFile("../assets/SimpleImages/Hawk_Feather.png");
		break;
	case 3:
		privateTexture.loadFromFile("../assets/SimpleImages/Eagle_Feather.png");
		break;
	}
	sprite = new sf::Sprite();
	sprite->setTexture(privateTexture);
	sprite->setPosition(0, 0);
	int tempSpread = rand() % 200 - 25;

	rect = new sf::FloatRect(0, 0,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	aliveTimer = 2;
	//std::cout << "Feather spawned" << std::endl;
}

void Feather::SetPosition(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
	aliveTimer = 2;
	speedX = rand() % 100 - 50;
	speedY = rand() % 100 - 50;
}

void Feather::Update(float deltatime)
{
	aliveTimer -= deltatime;
	rect->left -= speedX * deltatime * aliveTimer;
	rect->top += speedY * deltatime * aliveTimer;
	sprite->rotate(50 * deltatime * aliveTimer);
	sprite->setColor(sf::Color(255, 255, 255, 127 * aliveTimer));
	if (aliveTimer < 0) { SetPosition(-500, -500); }
}

sf::FloatRect* Feather::GetRect()
{
	return rect;
}

sf::Sprite* Feather::GetSprite()
{
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

int Feather::GetType() { return 2; }