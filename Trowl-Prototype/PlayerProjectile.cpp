#include "PlayerProjectile.h"
#include <iostream>
#include <SFML\Graphics.hpp>
#include <math.h>

#include "baseState.h"		  
#include "baseProjectile.h"



PlayerProjectile::PlayerProjectile() { std::cout << "PlayerProjectile spawned using wrong constructor" << std::endl; }
PlayerProjectile::~PlayerProjectile() {}

PlayerProjectile::PlayerProjectile(float xPos, float yPos, System* systemIn)
{
	system = systemIn;

	privateTexture.loadFromFile("../assets/Projectile1.png");
	sprite = new sf::Sprite();
	sprite->setTexture(privateTexture);
	sprite->setPosition(0, 0);

	sf::FloatRect* derp = new sf::FloatRect(xPos, yPos, sprite->getLocalBounds().width,sprite->getLocalBounds().height);
	rect = derp;	 

	speed = 1000.0f; 
}

void PlayerProjectile::activate(float xPos, float yPos) 
{					
	rect->left = xPos;
	rect->top = yPos;
}								

sf::FloatRect* PlayerProjectile::getRect() { return rect; }

void PlayerProjectile::Update(float deltatime)
{
	rect->left += (deltatime * speed * power);
	power -= deltatime;
}					    

sf::Sprite* PlayerProjectile::getSprite() 
{
	sprite->setPosition(rect->left,rect->top);
	if (power < 1) 
	{
		sprite->setColor(sf::Color(255, 255, 255, 255 * power)); // half transparent
	}
	else 
	{
		sprite->setColor(sf::Color(255, 255, 255, 255));
	}								 
	return sprite; 
}

void PlayerProjectile::setPower(float powerIn) { power = powerIn; }
float PlayerProjectile::getPower() { return power; }
int PlayerProjectile::getType() { return 1; }
