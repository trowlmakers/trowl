#pragma once
#include "baseObstacle.h"
#include <SFML\Graphics.hpp>  


class Tunnel : public baseObstacle
{
public:
	Tunnel();
	Tunnel(float xPos, float yPos);
	void Activate(float xPos, float yPos);
	~Tunnel();

	sf::Sprite* GetSprite();
	sf::FloatRect* GetRect();

	void Update(float deltaTime);

	int GetType();

	void Draw();

	void SetAudioOffset(float value);


private:
	sf::Texture privateTexture;
	sf::Sprite* sprite;
	sf::FloatRect* rect;
};

