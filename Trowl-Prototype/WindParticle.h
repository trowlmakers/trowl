#pragma once

#include <SFML\Graphics.hpp>
#include "baseParticle.h"

class AnimatedSprite;

class WindParticle : public baseParticle
{
public:
	WindParticle();
	~WindParticle();

	void SetPosition(float xPos, float yPos);

	void Update(float deltatime);

	sf::FloatRect* GetRect();
	sf::Sprite* GetSprite();

	int GetType();
private:
	float aliveTimer;
	float aliveTime;
	AnimatedSprite* sprite;
	sf::FloatRect* rect;
};

