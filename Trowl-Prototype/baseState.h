#pragma once
#include <vector>
#include "SFML\Graphics\RenderWindow.hpp"
#include "ProjectileManager.h"
#include "ObstacleManager.h"
#include "EnemyManager.h"
#include "ParticleManager.h"
#include "PowerupManager.h"
#include "SpawnChecker.h"
#include "OwletManager.h"
#include "FrozenScreen.h"

class Player;
class OwletManager;

class baseState
{
public:
	virtual ~baseState() {};
	virtual void Enter() = 0;
	virtual void Initiate(System* inputSystem) = 0;
	virtual bool Update(float deltaTime) = 0;
	virtual void Exit() =0;
	virtual int NextState() = 0;
	virtual int GetStateType() = 0; // outsidestate 0, insidestate 1, endstate 2, menustate 3
};


struct System
{
	sf::RenderWindow window;
	std::vector<baseState*> states;
	EnemyManager* enemyManager;
	ProjectileManager* projectileManager;
	ObstacleManager* obstacleManager;
	ParticleManager* particleManager;
	PowerupManager* powerupManager;
	SpawnChecker* spawnChecker;
	Player* player;
	OwletManager* owletManager;
	FrozenScreen* freezeScreen;
	
	int score;
	float audioLevel;
	bool isFullscreen = false;
	bool screenChange = false;
	int screenWidth;
	int screenHeight;

	bool hasWon = false;
};

