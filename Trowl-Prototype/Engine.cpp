#pragma once
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>

#include "Player.h"
#include "Engine.h"
#include "InsideState.h"
#include "baseState.h"
#include "OutsideState.h"
#include "EndState.h"
#include "MenuState.h"			 

#include "SpriteCreator.h"

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#pragma comment(lib, "sfml-audio.lib")
#endif


//what

Engine::Engine() {}	 
Engine::~Engine() {}	

bool Engine::Update() {
	sf::Time deltatime = clock.restart();
	frameTime = std::min(deltatime.asSeconds(), 0.1f);
	timeAccumulator += frameTime;

	while (timeAccumulator > targetTime)
	{
		//Code for switching between Fullscreen and Windowed
		if(system->screenChange){
			//If it already is Fullscreen, switch to windowed.
			if (system->isFullscreen) {
				system->window.create(sf::VideoMode(
					system->screenWidth, system->screenHeight),
					"Trowl", sf::Style::Titlebar | sf::Style::Resize | sf::Style::Close);
				system->isFullscreen = false;
			}
			//Else switch to Fullscreen
			else {
				system->window.create(sf::VideoMode(
					sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height),
					"Trowl", sf::Style::Fullscreen);
				system->isFullscreen = true;
			}
			system->screenChange = false; //Remember to turn of screenChange when the change is done
			system->window.setVerticalSyncEnabled(true);
		}


		sf::Event event;

		// while there are pending events...
		while (system->window.pollEvent(event))
		{
			// check the type of the event...
			switch (event.type)
			{
			// window closed
			case sf::Event::Closed:
				system->window.close();
				break;

			//Check for Keys pressed
			case sf::Event::KeyPressed:
				switch (event.key.code) {

				//Toggle Fullscreen
				case sf::Keyboard::U:
					system->screenChange = true;
					break;

				//Quit Program
				case sf::Keyboard::Escape:
					system->window.close();
				}
				

			// we don't process other types of events
			default:
				break;
			}
		}

		if (activeState->Update(targetTime) == false)
		{
			activeState->Exit();

			if (activeState->GetStateType() == 2) {
				Restart();
			}

			int searchFor = activeState->NextState();
			for (int i = 0; i < system->states.size(); i++) 
			{
				if (system->states[i]->GetStateType() == searchFor)
				{
					activeState = system->states[i];
				}
			}
			activeState->Enter();
		}
		timeAccumulator -= targetTime;
	}

	if (system->window.isOpen() == false)
	{
		return false;
	}
	return true;
}

bool Engine::Initialize() {
	activeState = nullptr;	  

	system = new System();
	system->screenWidth = 1280;
	system->screenHeight = 720;

	//Read the settings.txt, right now only to check if game should start in fullscreen or windowed
	std::ifstream settings("../assets/settings.txt");
	bool fullscreen; settings >> fullscreen;
	system->isFullscreen = fullscreen;
	settings.close();

	system->window.create(sf::VideoMode(
		system->screenWidth, system->screenHeight),
		"Trowl", sf::Style::Titlebar | sf::Style::Resize | sf::Style::Close);

	LoadingImage();

	system->player = new Player(
		system->screenWidth / 2,
		system->screenHeight / 2,
		system);

	system->enemyManager = new EnemyManager();
	system->enemyManager->Initialize(system);
	system->projectileManager = new ProjectileManager();
	system->projectileManager->Initialize(system);
	system->obstacleManager = new ObstacleManager();
	system->obstacleManager->Initialize(system);
	system->particleManager = new ParticleManager();
	system->particleManager->Initialize(system);
	system->powerupManager = new PowerupManager();
	system->powerupManager->Initialize(system);
	system->spawnChecker = new SpawnChecker();
	system->spawnChecker->Initialize(system);
	system->owletManager = new OwletManager();
	system->owletManager->Initialize(system);
	system->freezeScreen = new FrozenScreen();
	system->freezeScreen->Initialize(system);

	system->states.clear();

	OutsideState* state0 = new OutsideState();
	state0->Initiate(system);
	system->states.push_back(state0);
	InsideState* state1 = new InsideState();
	state1->Initiate(system);
	system->states.push_back(state1);
	EndState* state2 = new EndState();
	state2->Initiate(system);
	system->states.push_back(state2);
	MenuState* state3 = new MenuState();
	system->states.push_back(state3);
	state3->Initiate(system);

	state3->Enter();
	activeState = state3;

	targetTime = 1.0f / 60.0f;
	frameTime = 0.0f;
	timeAccumulator = 0.0f;
	sf::Clock clock;

	if (system->isFullscreen) {
		system->window.create(sf::VideoMode(
			sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height),
			"Trowl", sf::Style::Fullscreen);
	}

	system->window.setVerticalSyncEnabled(true);

	return true;		  
}

void Engine::Shutdown() {
	std::cout << "I did actually shut down" << std::endl;
	//Save current settings before quitting program, currently only saving if program is fullscreen or not
	std::ofstream settings("../assets/settings.txt");
	settings << system->isFullscreen; settings << std::endl;
	settings.close();
}


void Engine::Restart() {

	system->window.create(sf::VideoMode(
		system->screenWidth, system->screenHeight),
		"Trowl", sf::Style::Titlebar | sf::Style::Resize | sf::Style::Close);

	system->window.clear();
	LoadingImage();

	system->player->update(5);
	system->player->decreaseStress(100);
	system->player->setXPos(system->screenWidth / 2);
	system->player->setYPos(system->screenHeight / 2);

	delete system->spawnChecker;
	system->spawnChecker = new SpawnChecker;
	system->spawnChecker->Initialize(system);

	delete system->owletManager;
	system->owletManager = new OwletManager;
	system->owletManager->Initialize(system);
	
	for (int i = 0; i < system->states.size(); i++) {
		if (system->states[i]->GetStateType() == 0) {
			delete system->states[i];
			system->states.erase(system->states.begin()+i);
		}
	}

	OutsideState* state0 = new OutsideState;
	state0->Initiate(system);
	system->states.insert(system->states.begin(), state0);

	if (system->isFullscreen) {
		system->window.create(sf::VideoMode(
			sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height),
			"Trowl", sf::Style::Fullscreen);
	}

	system->window.setVerticalSyncEnabled(true);
}

void Engine::LoadingImage() {
	/*LOADING SCREEN HERE*/ {
		sf::Sprite loading;
		sf::Texture loadTex;
		loadTex.loadFromFile("../assets/loading.png");
		loading.setTexture(loadTex);
		system->window.draw(loading);
		system->window.display();
	}
}