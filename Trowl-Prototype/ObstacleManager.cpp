#include "ObstacleManager.h"

#include "baseObstacle.h"
#include "Branch.h"
#include "StateSwitch.h"
#include "ThunderCloud.h"
#include "Tunnel.h"
#include "TunnelWarning.h"
#include "Tree.h"

#include "Player.h"

#include "baseState.h" //for system struct
					   
#include <iostream>
					
#define PI 3.14159265


ObstacleManager::ObstacleManager() {}
ObstacleManager::~ObstacleManager() {}

void ObstacleManager::Initialize(System* inSystem)
{
	system = inSystem;

	drawRect.setFillColor(sf::Color(0, 255, 0, 125));
	drawRect.setOutlineColor(sf::Color(255, 0, 0));
	drawRect.setOutlineThickness(1);

	tunnelWarningArrow.loadFromFile("../assets/SimpleImages/arrow.png");

	//put obstacles in pool
	{
		StateSwitch* putinpool = new StateSwitch(0, 0);
		inactiveObstacles.push_back(putinpool);
		Branch* alsoputdis = new Branch(0, 0);
		inactiveObstacles.push_back(alsoputdis);
		ThunderCloud* thistoo = new ThunderCloud(0, 0);
		inactiveObstacles.push_back(thistoo);
		Tunnel* alsodat = new Tunnel(0, 0);
		inactiveObstacles.push_back(alsodat);
		Tree* anddis = new Tree(0, 0);
		inactiveObstacles.push_back(anddis);
	}
}

void ObstacleManager::Create(int selector, float xPos, float yPos)
{
	for (int i = 0; i < inactiveObstacles.size(); i++)
	{
		if (inactiveObstacles[i]->GetType() == selector)
		{
			activeObstacles.push_back(inactiveObstacles[i]);
			inactiveObstacles[i]->Activate(xPos, yPos);
			inactiveObstacles.erase(inactiveObstacles.begin() + i);
			std::cout << "Obstacle pulled from pool" << std::endl;
			return;	  
			//if we find something to use, we dont want to keep going.
		}
	}

	switch (selector) {
	case 1:
	{  		
		StateSwitch* insert = new StateSwitch(xPos, yPos);
		activeObstacles.push_back(insert);
		std::cout << "StateSwitch created" << std::endl;
	}
	break;
	case 2:
	{
		Branch* insert = new Branch(xPos, yPos);
		activeObstacles.push_back(insert);
		std::cout << "Branch created" << std::endl;
	}
	break;
	case 3:
	{
		ThunderCloud* insert = new ThunderCloud(xPos, yPos);
		activeObstacles.push_back(insert);
		std::cout << "Thundercloud created" << std::endl;
	}
	break;
	case 4:
	{
		Tunnel* insert = new Tunnel(xPos, yPos);
		activeObstacles.push_back(insert);
		std::cout << "Tunnel created" << std::endl;
	}
	break;
	case 5:
	{
		TunnelWarning* insert = new TunnelWarning(xPos, yPos);
		activeObstacles.push_back(insert);
		std::cout << "TunnelWarning created" << std::endl;
	}
	break;
	case 6:
	{
		Tree* insert = new Tree(xPos, yPos);
		activeObstacles.push_back(insert);
		std::cout << "Tree created" << std::endl;
	}
	break;
	}
}

//return false if we want to leave current state
bool ObstacleManager::Update(float deltaTime)
{
	movementTracker += 5 * deltaTime;
	if (movementTracker > 2 * PI) { movementTracker -= 2 * PI; }

	for (int i = 0; i < activeObstacles.size(); i++)


	{
		activeObstacles[i]->Update(deltaTime);

		//Obstacles vs Player
		if (activeObstacles[i]->GetRect()->intersects(*system->player->getRect())) 
		{				 
			switch (activeObstacles[i]->GetType()) {
			case 1:			 // hatch
				return false;
				break;
			case 2:			//Branch
				std::cout << "branch hit detected" << std::endl;
				system->player->hitByPhysical();
				//system->player->increaseStress(3.0);
				break;
			case 3:					//thunder
				std::cout << "Thunder hit detected" << std::endl;
				system->player->hitByThunder();
				break;
			case 4:				//tunnel
				std::cout << "Tunnel hit detected" << std::endl;
				system->player->hitByTunnel();
				break;
			}
		}

		//Obstacles vs Enemy
		switch (activeObstacles[i]->GetType())
		{
		case 2:
			system->enemyManager->obstacleIn(activeObstacles[i]->GetRect(), 1);
			break;
		case 3:
			system->enemyManager->obstacleIn(activeObstacles[i]->GetRect(), 2);
			break;
		case 5: //Sneaky warning sign stuff here!
			float center = system->window.getView().getCenter().x;
			float signPos = activeObstacles[i]->GetRect()->left;
			float deltaX = center - signPos;
			activeObstacles[i]->SetAudioOffset(deltaX);
			break;
		}


		if (system->player->getRect()->left - system->screenWidth > activeObstacles[i]->GetRect()->left)
		{	  //if out of bounds backward, clean up
			
			if (activeObstacles[i]->GetType() == 3)
			{ 
				std::cout << "THUNDER FUCKING CLEANED" << std::endl;
			}
			inactiveObstacles.push_back(activeObstacles[i]);
			activeObstacles.erase(activeObstacles.begin() + i);
			i--;
			
			std::cout << "Obstacle put into pool" << std::endl;
		}
	}	  
	return true;
}

void ObstacleManager::Draw() {
	for (int i = 0; i < activeObstacles.size(); i++) {
		if (activeObstacles[i]->GetSprite() != nullptr &&
			activeObstacles[i]->GetType() != 6)
		{
			system->window.draw(*activeObstacles[i]->GetSprite());
		}
	}
}

void ObstacleManager::DrawTrees()
{
	for (int i = 0; i < activeObstacles.size(); i++)
	{
		if (activeObstacles[i]->GetType() == 6)
		{
			system->window.draw(*activeObstacles[i]->GetSprite());
		}
	}
}

void ObstacleManager::Clear() {
	while (activeObstacles.size() > 0) {
		inactiveObstacles.push_back(activeObstacles[0]);
	}
}

void ObstacleManager::DrawHitBoxes() {	 
	for (int i = 0; i < activeObstacles.size(); i++) {
		//Set drawRect position and size then draw
		drawRect.setPosition(sf::Vector2f(
			activeObstacles[i]->GetRect()->left,
			activeObstacles[i]->GetRect()->top));
		drawRect.setSize(sf::Vector2f(
			activeObstacles[i]->GetRect()->width,
			activeObstacles[i]->GetRect()->height));

		system->window.draw(drawRect);
	}
}

void ObstacleManager::DrawWarning()
{
	for (int i = 0; i < activeObstacles.size(); i++)
	{
		if (activeObstacles[i]->GetType() == 1)
		{
			spriteOut.setTexture(tunnelWarningArrow);
			spriteOut.setPosition(
				activeObstacles[i]->GetRect()->left,
				activeObstacles[i]->GetRect()->top - 250 + sin(movementTracker) * 15);
			system->window.draw(spriteOut);
		}
	}
}

void ObstacleManager::Erase() {
	for (auto itr = activeObstacles.begin(); itr != activeObstacles.end(); )
	{
		inactiveObstacles.push_back(*itr);
		itr = activeObstacles.erase(itr);
	}
}

