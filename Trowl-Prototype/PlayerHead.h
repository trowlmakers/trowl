#pragma once

#include <SFML\Graphics.hpp>

struct System;
class AnimatedSprite;

class PlayerHead
{
public:
	PlayerHead();
	~PlayerHead();

	sf::Sprite* getSprite();

	void setGolden(bool set);

	void Update(float deltaTime);

	void Scream();

	void setRed(int redVal);

	void setHeadPosition(float xPos, float yPos);

private:
	bool golden;
	sf::Texture normalTexture;
	sf::Texture goldenTexture;
	sf::Sprite* sprite;

	AnimatedSprite* screamingSprite;
	int state;
	float internalTimer;
};
