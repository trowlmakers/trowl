#include "Camera.h"
#include <SFML\Window\Keyboard.hpp>
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <iostream>
#include "BaseState.h"
#include "Player.h"

float cameraOffset = 0;
int cameraDir = 1;

Camera::Camera(System * inputSystem, float xPos, float yPos)
{
	system = inputSystem;
	player = nullptr;

	sf::View cam(sf::FloatRect(xPos, yPos, system->window.getSize().x, system->window.getSize().y));
	camera = cam;
}

Camera::Camera(System* inputSystem, Player* playerObj){
	system = inputSystem;
	player = playerObj;

	sf::View cam(sf::FloatRect(0, 0, system->window.getSize().x, system->window.getSize().y));
	camera = cam;

	cameraOffset = player->getRect()->left + system->screenWidth / 4;
}

Camera::~Camera() {

}

void Camera::Update() {
	if (player != nullptr) {
		//if (system->player->getRect()->left < system->screenWidth / 4) {
			//minX = system->player->getRect()->left;
			camera.setCenter(cameraOffset, system->screenHeight / 2);
			cameraOffset++;

			if (system->player->getRect()->left > camera.getCenter().x - 400) {
				float extraSpeed = player->GetSpeed() / 3;
				if (extraSpeed < 0) extraSpeed = 0;
				cameraOffset += extraSpeed;
			}
		//}

			player->SetMaxSide(camera.getCenter().x - 640, camera.getCenter().x - 200 - (player->getRect()->width * 2));
	}

	system->window.setView(camera);
}

void Camera::SetCenter(float posX, float posY) {
	camera.setCenter(posX, posY);
}

void Camera::SetOffset(float i) {
	cameraOffset = i;
}