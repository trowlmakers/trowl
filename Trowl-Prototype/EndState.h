#pragma once

#include "baseState.h"
#include <SFML\Graphics\Font.hpp>

class MenuButton;

class EndState : public baseState
{
public:
	EndState();
	~EndState();
	bool Update(float deltaTime);
	void Initiate(System* system);
	void Enter();
	void Exit();
 int NextState();
	int GetStateType();
	Player* getPlayer();
	void setPlayer(Player* inputPlayer);
private:			   

	sf::Font* textFont;
	System* system;

	sf::Texture bgTex;
	sf::Sprite* bg;

	sf::Texture scoreTex;
	sf::Sprite* score;
};

