#pragma once
#include <SFML\Graphics.hpp>
#include "baseEnemy.h"	

class Player;
class AnimatedSprite;
struct System;

class Human2 : public baseEnemy
{
public:
	Human2();
	~Human2();

	Human2(float xPos, float yPos, System* systemIn);
	void Activate(float xPos, float yPos);
	void Update(float deltatime);


	sf::FloatRect* getRect();
	sf::Sprite* getSprite();

	void hit(float power);
	void hitLightning();
	void hitBranch();
	bool collides();
	void flee();

	void activate();

	void setPlayer(Player * playerIn);

	int getType();

private:
	sf::Texture idleTexture;
	sf::Sprite* idleSprite;
	AnimatedSprite* throwingSprite;
	AnimatedSprite* windDownSprite;

	sf::Sprite* sprite;
	Player* player;
	sf::FloatRect* rect;

	System* system;

	float speed;
	bool colliding;
	int state;
	float internalTimer;
	float throwingTimer;
	float throwingFrequency;
};

