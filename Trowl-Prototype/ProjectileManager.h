#pragma once

#include <SFML\Graphics.hpp>

struct System;
class baseEnemy;
class Player;
class baseProjectile;

class ProjectileManager
{
public:
	ProjectileManager();
	~ProjectileManager();

	void Initialize(System* inSystem);

	void Update(float deltaTime);

	void Create(int selector, float xPos, float yPos, float powerIn);

	void Draw();

	void checkCollisions();

	float CalculateAngle(float, float);

	void Erase();

	void setPlayer(Player* inPlayer);

private:
	System* system;

	std::vector<baseProjectile*> activeProjectiles;
	std::vector<baseProjectile*> inactiveProjectiles;
};

