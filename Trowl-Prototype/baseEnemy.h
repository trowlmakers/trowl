#pragma once

#include <SFML\Graphics.hpp>

class baseEnemy
{
public:
	virtual ~baseEnemy() {};
	virtual void Activate(float xPos, float yPos) = 0;
	virtual void Update(float deltatime) = 0;
	virtual sf::FloatRect* getRect() = 0;
	virtual void hit(float power) = 0;
	virtual bool collides() = 0;
	virtual sf::Sprite* getSprite() = 0;
	virtual int getType() = 0;
	virtual void flee() = 0;
	virtual void hitLightning() = 0;
	virtual void hitBranch() = 0;
};

