#pragma once

#include <SFML\Graphics.hpp>
#include "baseParticle.h"


struct System;
class Player;


class ParticleManager
{
public:
	ParticleManager();
	~ParticleManager();

	void Initialize(System* inSystem);

	void Create(int selector, float xPos, float yPos);

	void Update(float deltaTime);

	void Draw();

	void checkCollisions();
private:
	float spawnTimer;
	float spawnFrequency;

	System* system;

	std::vector<baseParticle*> activeParticles;
	std::vector<baseParticle*> inactiveParticles;
};

