#include "Tree.h"
#include <iostream>

#include <SFML\Graphics.hpp>

Tree::Tree() { std::cout << "Tree created using wrong constructor" << std::endl; }
Tree::~Tree() {	}

Tree::Tree(float xPos, float yPos) {
	treeTex.loadFromFile("../assets/SimpleImages/Danger_tree.png");
	treeSpr = new sf::Sprite();
	treeSpr->setTexture(treeTex);
	treeSpr->setPosition(0, 0);
	rect = new sf::FloatRect(
		xPos,
		yPos,
		treeSpr->getLocalBounds().width,
		treeSpr->getLocalBounds().height);
}

void Tree::Activate(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
}

sf::Sprite* Tree::GetSprite() {
	treeSpr->setPosition(rect->left, rect->top);
	return treeSpr;
}

sf::FloatRect* Tree::GetRect() { return rect; }

void Tree::Update(float deltaTime) { rect->left -= deltaTime * 500; }

int Tree::GetType()
{
	return 6;
}

void Tree::SetAudioOffset(float value)
{
}
