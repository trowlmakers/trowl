#include "MenuState.h"
#include "MenuButton.h"
#include "Player.h"
#include "Camera.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
															   
#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

Camera* menuCamera;
sf::Texture fullscreenTex;
sf::Sprite* fullscreen;
bool firstTime = true;

MenuState::MenuState() {}
MenuState::~MenuState() {}

void MenuState::Initiate(System* inputSystem) {
	system = inputSystem;

	menuCamera = new Camera(system, 0, 0);

	fullscreenTex.loadFromFile("../assets/menu-images/Fullscreen.png");
	fullscreen = new sf::Sprite;
	fullscreen->setTexture(fullscreenTex);
	fullscreen->setPosition(system->screenWidth / 2 - fullscreen->getLocalBounds().width / 2 - 100,
		400);

	//create background	with scaling
	{
		skyTexture = new sf::Texture();
		skyTexture->loadFromFile("../assets/SimpleImages/MenuSky.png");

		skySprite = new sf::Sprite();
		skySprite->setTexture(*skyTexture);
		skySprite->setPosition(0, 0);

		sf::FloatRect spriteRect = skySprite->getLocalBounds();
		float scaleX = system->screenWidth / spriteRect.width;
		float scaleY = system->screenHeight / spriteRect.height;

		skySprite->setScale(scaleX, scaleY);
	}

	inactiveSoundButton.loadFromFile("../assets/menu-images/Volume_indicator.png");
	activeSoundButton.loadFromFile("../assets/menu-images/Volume_indicator_highlighted.png");

	bigBoardTexture.loadFromFile("../assets/menu-images/Leaderboard_options_backdrop.png");
	bigBoard.setTexture(bigBoardTexture);
	bigBoard.setPosition(0,0);

	smallBoardTexture.loadFromFile("../assets/menu-images/Optionsboard.png");
	smallBoard.setTexture(smallBoardTexture);
	smallBoard.setPosition(
		system->screenWidth / 2 - smallBoard.getLocalBounds().width / 2,
		350);

	scoreTextTexture.loadFromFile("../assets/menu-images/Leaderboard_title.png");
	scoreText.setTexture(scoreTextTexture);
	scoreText.setPosition(
		system->screenWidth / 2 - scoreText.getLocalBounds().width / 2,
		30);
	
	optionsTextTexture.loadFromFile("../assets/menu-images/Options_title.png");
	optionsText.setTexture(optionsTextTexture);
	optionsText.setPosition(
		system->screenWidth / 2 - optionsText.getLocalBounds().width / 2,
		30);

	MenuButton* backButton = new MenuButton(
		"../assets/menu-images/Back_button.png",
		"../assets/menu-images/Back_button_hoover.png");
	backButton->setButtonPosition(
		system->screenWidth - backButton->getRect()->width - 125,
		system->screenHeight - backButton->getRect()->height - 20);
	backButton->setType(0);
	backButton->setWindow(system);
	buttons.push_back(backButton);


	MenuButton* startButton = new MenuButton(
		"../assets/menu-images/Start_button.png",
		"../assets/menu-images/Start_button_hoover.png");
	startButton->setButtonPosition(
		system->screenWidth / 2 - startButton->getRect()->width / 2,
		system->screenHeight - 350);
	startButton->setType(1);
	startButton->setWindow(system);
	buttons.push_back(startButton);

	MenuButton* optionsButton = new MenuButton(
		"../assets/menu-images/Options_button.png",
		"../assets/menu-images/Options_button_hoover.png");
	optionsButton->setButtonPosition(
		system->screenWidth / 2 - optionsButton->getRect()->width / 2,
		system->screenHeight - 275);
	optionsButton->setType(2);
	optionsButton->setWindow(system);
	buttons.push_back(optionsButton);

	MenuButton* leaderBoardButton = new MenuButton(
		"../assets/menu-images/Score_button.png",
		"../assets/menu-images/Score_button_hoover.png");
	leaderBoardButton->setButtonPosition(
		system->screenWidth / 2 - leaderBoardButton->getRect()->width / 2,
		system->screenHeight - 220);
	leaderBoardButton->setType(3);
	leaderBoardButton->setWindow(system);
	buttons.push_back(leaderBoardButton);

	MenuButton* quitButton = new MenuButton(
		"../assets/menu-images/Exit_button.png",
		"../assets/menu-images/Exit_button_hoover.png");
	quitButton->setButtonPosition(
		system->screenWidth / 2 - quitButton->getRect()->width / 2,
		system->screenHeight - 165);
	quitButton->setType(7);
	quitButton->setWindow(system);
	buttons.push_back(quitButton);

	MenuButton* VolumeUpButton = new MenuButton(
		"../assets/menu-images/Plus_icon.png",
		"../assets/menu-images/Plus_icon_hoover.png");
	VolumeUpButton->setButtonPosition(
		system->screenWidth / 2 - VolumeUpButton->getRect()->width / 2 + 200,
		175);
	VolumeUpButton->setType(4);
	VolumeUpButton->setWindow(system);
	buttons.push_back(VolumeUpButton);

	MenuButton* VolumeDownButton = new MenuButton(
		"../assets/menu-images/Minus_icon.png",
		"../assets/menu-images/Minus_icon_hoover.png");
	VolumeDownButton->setButtonPosition(
		system->screenWidth / 2 - VolumeDownButton->getRect()->width / 2 - 200,
		175);
	VolumeDownButton->setType(5);
	VolumeDownButton->setWindow(system);
	buttons.push_back(VolumeDownButton);


	MenuButton* FullscreenButton;

	if (!system->isFullscreen) {
		FullscreenButton = new MenuButton(
			"../assets/menu-images/Fullscreen tickbox.png",
			"../assets/menu-images/Fullscreen tickbox.png");
	}
	else {
		FullscreenButton = new MenuButton(
			"../assets/menu-images/Fullscreen tickbox ON.png",
			"../assets/menu-images/Fullscreen tickbox ON.png");
	}
	FullscreenButton->setButtonPosition(
		system->screenWidth / 2 - FullscreenButton->getRect()->width / 2 + 200,
		400);
	FullscreenButton->setType(6);
	FullscreenButton->setWindow(system);
	buttons.push_back(FullscreenButton);   

	MenuButton* CreditsButton = new MenuButton(
		"../assets/menu-images/Credits_button.png",
		"../assets/menu-images/Credits_button_hoover.png");
	CreditsButton->setButtonPosition(
		system->screenWidth - CreditsButton->getRect()->width - 605,
		system->screenHeight - CreditsButton->getRect()->height - 150);
	CreditsButton->setType(8);
	CreditsButton->setWindow(system);
	buttons.push_back(CreditsButton);	

	for (int i = 0; i < 10; i++)
	{
		sf::Sprite newButton;
		if (i < 5) { newButton.setTexture(activeSoundButton); }
		else { newButton.setTexture(inactiveSoundButton); }
		newButton.setPosition(550 + i * 20, 160);
		soundButtons.push_back(newButton);
	}			

	hasBackground = true;
	state = 0;
	volumeLevel = 4;

	std::ifstream infile("../assets/HighScore.txt");
	for (int i = 0; i < 5; i++)
	{
		int tempscore; infile >> tempscore;
		scores.push_back(tempscore);
	}
	infile.close();
}

bool MenuState::Update(float deltaTime) {
	system->window.clear();
	menuCamera->Update();

	if (hasBackground) { system->window.draw(*skySprite); }
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P)) { return false; }

	//draw all buttons
/*	for (int i = 0; i < buttons.size(); i++)
	{
		system->window.draw(*buttons[i]->getSprite());
	}
*/
					 
	switch (state)
	{
	case 0: //Main state  	 
		system->window.draw(smallBoard);

		for (int i = 0; i < buttons.size(); i++)
		{								  // we only want to run the buttons active in the state
			if (buttons[i]->getType() == 1 ||
				buttons[i]->getType() == 2 ||
				buttons[i]->getType() == 3 ||
				buttons[i]->getType() == 7)
			{
				system->window.draw(*buttons[i]->getSprite());
				if (buttons[i]->update())	 // if the button is pressed, do action
				{

					if (buttons[i]->getType() == 1)	 //start button
					{
						return false;
					}
					if (buttons[i]->getType() == 2) // option button
					{
						state = 1;
					}
					if (buttons[i]->getType() == 3) // highscore button
					{
						state = 2;
					}
					if (buttons[i]->getType() == 7) // quit button
					{
						system->window.close();
					}
				}
			}
		}

		break;
	case 1: //Options State
		system->window.draw(bigBoard);
		system->window.draw(optionsText);
		system->window.draw(*fullscreen);

		for (int i = 0; i < soundButtons.size(); i++)
		{
			system->window.draw(soundButtons[i]);
		}

		for (int i = 0; i < buttons.size(); i++)
		{								  // we only want to run the buttons active in the state
			if (buttons[i]->getType() == 0 ||
				buttons[i]->getType() == 4 ||
				buttons[i]->getType() == 5 ||
				buttons[i]->getType() == 6 ||
				buttons[i]->getType() == 8)
			{
				system->window.draw(*buttons[i]->getSprite());

				if (buttons[i]->update())	 // if the button is pressed, do action
				{
					if (buttons[i]->getType() == 0) //move back button
					{
						state = 0;
					}
					if (buttons[i]->getType() == 4)     //sound up
					{
						if (volumeLevel < 9)
						{
							volumeLevel++;
							soundButtons[volumeLevel].setTexture(activeSoundButton);
							sf::Listener::setGlobalVolume(10 * volumeLevel);
						}
					}
					if (buttons[i]->getType() == 5) // sound down
					{
						if (volumeLevel > 0)
						{
							soundButtons[volumeLevel].setTexture(inactiveSoundButton);
							volumeLevel--;
							sf::Listener::setGlobalVolume(10 * volumeLevel);
						}
					}
					if (buttons[i]->getType() == 6) // Toggle Fullscreen
					{
						if (system->isFullscreen) {
							system->screenChange = true;
							buttons[i]->SetSprite("../assets/menu-images/Fullscreen tickbox.png");
							std::cout << "1" << std::endl;
						}
						else {
							system->screenChange = true;
							buttons[i]->SetSprite("../assets/menu-images/Fullscreen tickbox ON.png");
							std::cout << "2" << std::endl;
						}
					}
					if (buttons[i]->getType() == 8)
					{
						system->freezeScreen->Freeze("../assets/SimpleImages/Credits.png");
					}
					while (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {}
				}
			}
		}
		break;

	case 2: //HighScore State
	{
		system->window.draw(bigBoard);
		system->window.draw(scoreText);
		//Display HighScore
		sf::Font textFont;
		if (!textFont.loadFromFile("../assets/font.ttf"))
		{
			std::cout << "error loading font" << std::endl;
		}
		sf::Text textOut = sf::Text();
		textOut.setFont(textFont);
		textOut.setCharacterSize(30);
		textOut.setColor(sf::Color::Black);

		for (int i = 0; i < scores.size(); i++)
		{
			textOut.setPosition(
				sf::Vector2f(
					system->screenWidth / 2,
					system->screenHeight / 2 + 50 * (i - 3)));

			std::string Result;          // string which will contain the result  
			std::ostringstream convert;   // stream used for the conversion          
			convert << scores[i];      // insert the textual representation of 'Number' in the characters in the stream
			Result = convert.str(); // set 'Result' to the contents of the stream
			textOut.setString(Result);
			system->window.draw(textOut);
		}


		for (int i = 0; i < buttons.size(); i++)
		{								  // we only want to run the buttons active in the state
			if (buttons[i]->getType() == 0)
			{
				if (buttons[i]->update())	 // if the button is pressed, do action
				{
					if (buttons[i]->getType() == 0) //move back button
					{
						state = 0;
					}
				}
				system->window.draw(*buttons[i]->getSprite());
			}
		}
	}
			break;

	case 3:	  //etc...
		break;		 		
	}
	system->window.display();
	return true;
}
					
void MenuState::Enter() {
	music.openFromFile("../assets/AUDIO/Intro_pictures_music.wav");
	music.setLoop(true);
	music.setVolume(100);
	music.play();

	system->score = 0;

	system->player->decreaseStress(100);
	system->player->update(1);
	if (firstTime) {
		system->freezeScreen->Freeze("../assets/SimpleImages/Story_image1.png");
		system->freezeScreen->Freeze("../assets/SimpleImages/Story_image2.png");
		system->freezeScreen->Freeze("../assets/SimpleImages/Story_image3.png");
		system->freezeScreen->Freeze("../assets/SimpleImages/Story_image4.png");
		system->freezeScreen->Freeze("../assets/SimpleImages/Story_image5.png");
		system->freezeScreen->Freeze("../assets/SimpleImages/Story_image6.png");
		firstTime = false;
	}
}

void MenuState::Exit() 
{					 	
	system->freezeScreen->Freeze("../assets/SimpleImages/Tutorial_screen_1.png");
	system->freezeScreen->Freeze("../assets/SimpleImages/Tutorial_screen_2.png");

	music.stop();
}

int MenuState::NextState()
{
	return 0;
}

int MenuState::GetStateType() { return 3; }
Player* MenuState::getPlayer() { return nullptr; }

void MenuState::setPlayer(Player* inputPlayer) {}

