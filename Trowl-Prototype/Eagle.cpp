#pragma once

#include "Eagle.h"
#include "Player.h"
#include "AnimatedSprite.h"

#include <SFML\Audio.hpp>

Eagle::Eagle() {}
Eagle::~Eagle() {}

Eagle::Eagle(float xPos, float yPos) {
	pos.x = xPos;
	pos.y = yPos;
	
	idleSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Eagle_flap_sprite_sheet.png",
		"../assets/AnimatedObjects/Eagle_flap_sprite_sheet.txt");
	divingSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/eagle_dive_sprite_sheet.png",
		"../assets/AnimatedObjects/eagle_dive_sprite_sheet.txt"
		);
	elecSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/eagle_elec_sprite_sheet.png",
		"../assets/AnimatedObjects/eagle_elec_sprite_sheet.txt");

	hitSound.loadFromFile("../assets/AUDIO/EnemyEagleHit.wav");
	fleeSound.loadFromFile("../assets/AUDIO/EnemyRetreat.wav");
	diveSound.loadFromFile("../assets/AUDIO/FalconDiveEnemy.wav");
	elecSound.loadFromFile("../assets/AUDIO/Electrocution.wav");

	sprite = idleSprite->getSprite();

	rect = new sf::FloatRect(
		xPos,
		yPos + sprite->getLocalBounds().height / 2 + 10,
		sprite->getLocalBounds().width,
		35);
	speed = 200;
	state = 0;
	internalTimer = 0;
	colliding = true;
	hitPoints = 3;
}			  

void Eagle::Activate(float xPos, float yPos)
{
	pos.x = xPos;
	pos.y = yPos;

	sprite = idleSprite->getSprite();
	rect->top = pos.y + sprite->getLocalBounds().height / 2 + 10;
	rect->left = xPos;
	rect->width = sprite->getLocalBounds().width;
	rect->height = 35;

	colliding = true;
	speed = 200;
	hitPoints = 3;
	state = 0;
	internalTimer = 0;
}

void Eagle::Update(float deltatime) {
	switch (state)
	{
	case 0: //normal, detect player
		pos.x -= speed*deltatime;
		
		idleSprite->update(deltatime);
		sprite = idleSprite->getSprite();

		rect->left = pos.x;	   

		if (rect->left < player->getRect()->left + player->getRect()->width / 2 &&
			rect->left > player->getRect()->left &&
			rect->top < player->getRect()->top) 
		{
			speed *= 4;
			state = 3;
			sprite = divingSprite->getSprite();
			rect->width = sprite->getLocalBounds().width;
			rect->height = sprite->getLocalBounds().height;
		}

		break;
	case 1:	 //fleeing			
		idleSprite->update(deltatime);
		sprite = idleSprite->getSprite();

		pos.x -= speed* deltatime;
		pos.y -= speed* deltatime;

		rect->left = pos.x;
		rect->top = pos.y;
		break;
	case 2:	  //hitstun
		internalTimer -= deltatime;
		pos.x += internalTimer * 20;
		if (internalTimer < 0)
		{
			if (hitPoints > 0)
			{
				state = 0;
			}
			else
			{
				soundOut.setBuffer(fleeSound);
				soundOut.play();
				state = 1;
				colliding = false;
			}
		}			
		break;
	case 3: //dropping towards player
		divingSprite->update(deltatime);
		sprite = divingSprite->getSprite();
		pos.y += speed*deltatime;
		rect->top = pos.y;
		break;
	case 4: //elecStun
		internalTimer -= deltatime;
		if (internalTimer <= 0)
		{
			pos.x = -50;
			state = 1;
		}

		elecSprite->update(deltatime);
		sprite = elecSprite->getSprite();
	}
}

sf::FloatRect * Eagle::getRect()
{
	rect->left = pos.x;
	return rect;
}

sf::Sprite * Eagle::getSprite()
{
	sprite->setPosition(pos.x, pos.y);
	return sprite;
}

void Eagle::hit(float power)
{
	soundOut.setBuffer(hitSound);
	soundOut.play();
	hitPoints -= power;
	state = 2;
	internalTimer = power / 2;
}

void Eagle::hitLightning()
{
	if (state != 4)
	{
		internalTimer = 0.78;
	}

	state = 4;
}	 

void Eagle::hitBranch() 
{	   
	soundOut.setBuffer(hitSound);
	soundOut.play();
	hitPoints -= 3;
	state = 2;
	internalTimer = 0.5;
}

void Eagle::flee()
{			  
	state = 1;
	colliding = false;
}

bool Eagle::collides()
{
	return colliding;
}

void Eagle::activate()
{
	state = 0;
	colliding = true;
}

void Eagle::setPlayer(Player* inPlayer)
{
	player = inPlayer;
}
int Eagle::getType() { return 1; }
