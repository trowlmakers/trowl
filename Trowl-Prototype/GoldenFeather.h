#pragma once
#include "basepowerup.h"
#include "SFML/Graphics.hpp"

class AnimatedSprite;

class GoldenFeather : public basePowerUp
{

public:
	GoldenFeather();
	GoldenFeather(float xPos, float yPos);
	~GoldenFeather() {};
	void Update(float deltaTime);

	sf::FloatRect* getRect();
	sf::Sprite* getSprite();

	int getType();

	void FadeOut();

private:
	AnimatedSprite* sprite;
	sf::FloatRect* rect;
	bool fading = false;
	float internalTimer;
};

