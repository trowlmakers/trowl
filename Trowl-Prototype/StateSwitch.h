#pragma once
#include "baseObstacle.h"
#include <SFML\Graphics.hpp>  


class StateSwitch	: public baseObstacle
{
public:
	StateSwitch();
	StateSwitch(float xPos, float yPos);
	~StateSwitch();

	void Activate(float xPos, float yPos);

	sf::Sprite* GetSprite();
	sf::Sprite * GetSpriteInside();
	sf::FloatRect* GetRect();

	void Update(float deltaTime);

	int GetType();

	void SetRect(float x, float y);

	void Draw(System * system);

	void SetAudioOffset(float value);


private:
	sf::Texture privateTexture;
	sf::Sprite* sprite;
	sf::FloatRect* rect;
};

