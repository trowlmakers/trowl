#include "ParticleManager.h"
#include "Player.h"

#include "baseParticle.h"
#include "Leaf.h"
#include "Feather.h"
#include "WindParticle.h"

#include "baseState.h"

#include <iostream>

ParticleManager::ParticleManager() {}
ParticleManager::~ParticleManager() {}

void ParticleManager::Initialize(System * inSystem)
{
	system = inSystem;
	spawnTimer = 0;
	spawnFrequency = 0.1;

	for (int i = 0; i < 5; i++) {
		WindParticle* newWind = new WindParticle();
		newWind->SetPosition(-500,-500);
		activeParticles.push_back(newWind);
	}

}

void ParticleManager::Create(int selector, float xPos, float yPos)
{
	float LeafHeight = rand() % system->screenHeight;

	for (int i = 0; i < inactiveParticles.size(); i++)
	{					  //search if what we want to create exists in pool
		if (inactiveParticles[i]->GetType() == selector)
		{
			switch (selector)
			{
			case 1:
				inactiveParticles[i]->SetPosition(
					system->player->getRect()->left + system->screenWidth -
					system->player->getRect()->width,
					LeafHeight);
				break;
			default:
				//if (system->powerupManager->CheckPowerUp() == 4)
				{
					inactiveParticles[i]->SetPosition(xPos, yPos);
				}
				break; 
			}
			activeParticles.push_back(inactiveParticles[i]);
			inactiveParticles.erase(inactiveParticles.begin() + i);
			return;
		}
	}

	switch (selector)
	{
	case 1:
	{
		Leaf* newLeaf = new Leaf();
		newLeaf->SetPosition(
			//	system->window.mapPixelToCoords(sf::Vector2i(system->screenWidth, 0)).x
			system->player->getRect()->left + system->screenWidth -
			system->player->getRect()->width,
			LeafHeight);
		activeParticles.push_back(newLeaf);
	}		   
	break;
	case 2:
	{
		Feather* newFeather = new Feather();
		newFeather->SetPosition(xPos, yPos);
		activeParticles.push_back(newFeather);
		break;
	}
	case 3:
	{
		WindParticle* newWind = new WindParticle();
		newWind->SetPosition(xPos, yPos);
		activeParticles.push_back(newWind);
		break;
	}		  
	}
}

void ParticleManager::Update(float deltaTime)
{
	spawnTimer += deltaTime;
	while (spawnTimer > spawnFrequency)
	{
		spawnTimer -= spawnFrequency;
		Create(1,0,0);

		int random1 = rand() % system->screenWidth + system->player->getRect()->left;
		int random2 = rand() % system->screenHeight;
		if (system->powerupManager->CheckPowerUp() == 4)
		{
			Create(3, random1, random2);
		}
		//std::cout << "Leaf timer spawn" << std::endl;
	}

	for (int i = 0; i < activeParticles.size(); i++) {
		activeParticles[i]->Update(deltaTime);
		if (system->powerupManager->CheckPowerUp() == 4) { activeParticles[i]->Update(deltaTime); }
		if (
			system->player->getRect()->left - system->screenWidth > activeParticles[i]->GetRect()->left ||
			activeParticles[i]->GetRect()->top > system->screenHeight ||
			activeParticles[i]->GetRect()->top < 0 - activeParticles[i]->GetRect()->height)
		{
			inactiveParticles.push_back(activeParticles[i]);
			activeParticles.erase(activeParticles.begin() + i);
			i--;
			//std::cout << "particle put in pool" << std::endl;
		}
	}
}

void ParticleManager::Draw()
{
	for (int i = 0; i < activeParticles.size(); i++) 
	{
		system->window.draw(*activeParticles[i]->GetSprite());
	}
}
void ParticleManager::checkCollisions()
{
}

