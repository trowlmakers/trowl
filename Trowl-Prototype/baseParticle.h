#pragma once

#include <SFML\Graphics.hpp>

class baseParticle
{
public:
	baseParticle();
	~baseParticle();

	virtual void SetPosition(float xPos, float yPos) = 0;
	virtual void Update(float deltaTime) = 0;		   
	virtual sf::FloatRect* GetRect() = 0;
	virtual sf::Sprite* GetSprite() = 0;   
	virtual int GetType() = 0;


};

