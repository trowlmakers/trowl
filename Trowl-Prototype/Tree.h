#pragma once
#include "baseObstacle.h"
#include <SFML\Graphics.hpp>  

class System;


class Tree : public baseObstacle
{
public:
	Tree();
	Tree(float xPos, float yPos);
	void Activate(float xPos, float yPos);
	~Tree();

	sf::Sprite* GetSprite();
	sf::FloatRect* GetRect();

	void Update(float deltaTime);

	int GetType();

	void SetAudioOffset(float value);


private:
	sf::Texture treeTex;
	sf::Sprite* treeSpr;
	sf::FloatRect* rect;
};

