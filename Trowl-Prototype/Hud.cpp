#include "Hud.h"
#include <iostream>
#include "Player.h"
#include "SpriteCreator.h"	
#include "baseState.h"

#include "sstream"

#include <SFML\Graphics.hpp>
#include <SFML/Window.hpp>

Hud::~Hud() {}		
Hud::Hud()
{	
}

void Hud::Initialize(System* systemIn)
{
	system = systemIn;	   

	stressContainerTexture.loadFromFile("../assets/stress_bar.png");
	stressFillerTexture.loadFromFile("../assets/stress_juice.png");
	
	stressContainerSprite.setTexture(stressContainerTexture);
	stressFillerSprite.setTexture(stressFillerTexture);

	if (!textFont.loadFromFile("../assets/font.ttf")) { std::cout << "error loading font" << std::endl; }
												
	textOut.setFont(textFont);
	textOut.setCharacterSize(30);
	textOut.setColor(sf::Color::White);
}

void Hud::Update()
{		
	currentStress = system->player->getStress();
}

void Hud::Draw()
{		

	stressContainerSprite.setPosition(system->window.mapPixelToCoords(sf::Vector2i(10,10)));
	system->window.draw(stressContainerSprite);

	stressFillerSprite.setPosition(system->window.mapPixelToCoords(sf::Vector2i(13, 11)));
	int fillamount = stressContainerTexture.getSize().x * (currentStress / 10);
	stressFillerSprite.setTextureRect(sf::IntRect(0,0,
		fillamount,
		stressFillerTexture.getSize().y));
					
	system->window.draw(stressFillerSprite);

	std::string Result;          // string which will contain the result  
	std::ostringstream convert;   // stream used for the conversion		  
	convert << system->score;      // insert the textual representation of 'Number' in the characters in the stream
	Result = convert.str(); // set 'Result' to the contents of the stream
	textOut.setString(Result);
	textOut.setPosition(system->window.mapPixelToCoords((sf::Vector2i(system->screenWidth / 2, 10))));
	system->window.draw(textOut);
}

