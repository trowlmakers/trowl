#include "StateSwitch.h"
#include <iostream>

#include <SFML\Graphics.hpp>

StateSwitch::StateSwitch() { std::cout << "Stateswitch created using wrong constructor" << std::endl; }
StateSwitch::~StateSwitch() {	}

StateSwitch::StateSwitch(float xPos, float yPos) {
	privateTexture.loadFromFile("../assets/Hatch.png");
	sprite = new sf::Sprite();
	sprite->setTexture(privateTexture);

	rect = new sf::FloatRect(
		xPos,
		yPos,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	sprite->setPosition(rect->left, rect->top);
}										

void StateSwitch::Activate(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
}

sf::Sprite* StateSwitch::GetSprite() { 
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

sf::Sprite* StateSwitch::GetSpriteInside() {
	return sprite;
}

sf::FloatRect* StateSwitch::GetRect() { return rect; }

void StateSwitch::Update(float deltaTime) {}

int StateSwitch::GetType()
{
	return 1;
}

void StateSwitch::SetRect(float x, float y) {
	rect = new sf::FloatRect(
		x,
		y,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);
}

void StateSwitch::SetAudioOffset(float value)
{
}
