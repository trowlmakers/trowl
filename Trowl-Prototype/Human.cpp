#include "Human.h"
#include "Player.h"

#include "AnimatedSprite.h"
#include "baseState.h"

#include <iostream>

Human::Human() {}
Human::~Human() {}

Human::Human(float xPos, float yPos, System* systemIn) {
	system = systemIn;
	speed = 0;


	//generate sprites
	{
		int i = rand() % 3 + 1;
		switch (i)
		{
		case 1:
		{

			idleTexture.loadFromFile("../assets/simpleImages/Hooman1.2.png");
			throwingSprite = new AnimatedSprite(
				"../assets/AnimatedObjects/Human1ChargeUp_SpSh.png",
				"../assets/AnimatedObjects/Human1ChargeUp_SpSh.txt");

			windDownSprite = new AnimatedSprite(
				"../assets/AnimatedObjects/Human1ThrowRelease_SpSH.png",
				"../assets/AnimatedObjects/Human1ThrowRelease_SpSH.txt");
		}
		break;
		case 2:
		{

			idleTexture.loadFromFile("../assets/simpleImages/Hooman2.2.png");
			throwingSprite = new AnimatedSprite(
				"../assets/AnimatedObjects/Human2ChargeUp_SpSh.png",
				"../assets/AnimatedObjects/Human2ChargeUp_SpSh.txt");

			windDownSprite = new AnimatedSprite(
				"../assets/AnimatedObjects/Human2ThrowRelease_SpSH.png",
				"../assets/AnimatedObjects/Human2ThrowRelease_SpSH.txt");
		}
		break;
		case 3:
		{

			idleTexture.loadFromFile("../assets/simpleImages/Hooman3.2.png");
			throwingSprite = new AnimatedSprite(
				"../assets/AnimatedObjects/Human3ChargeUp_SpSh.png",
				"../assets/AnimatedObjects/Human3ChargeUp_SpSh.txt");

			windDownSprite = new AnimatedSprite(
				"../assets/AnimatedObjects/Human3ThrowRelease_SpSH.png",
				"../assets/AnimatedObjects/Human3ThrowRelease_SpSH.txt");
		}
		break;
		}
	}

		idleSprite = new sf::Sprite;
		//idleSprite->setScale(3, 3);
		idleSprite->setTexture(idleTexture);
		idleSprite->setPosition(0, 0);
		sprite = idleSprite;


	rect = new sf::FloatRect(xPos, yPos,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	state = 0;
	internalTimer = 0;
	throwingTimer = 0;
	throwingFrequency = 3 + rand() % 5;
	colliding = false;

}
void Human::Activate(float xPos, float yPos)
{
	rect = new sf::FloatRect(xPos, yPos,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	state = 0;
	internalTimer = 0;
	throwingTimer = 0;
	throwingFrequency = 3;
	colliding = false;
}

void Human::Update(float deltatime) {
	switch (state)
	{
	case 0: //idle
		{
			throwingTimer += deltatime;
			if (throwingTimer > throwingFrequency) { 
				throwingTimer -= throwingFrequency + rand() % 5;
				state = 1; 
			}
		}
		break;
	case 1: //throwing 
		throwingSprite->update(deltatime);
		sprite = throwingSprite->getSprite();
		internalTimer += deltatime;
		if (internalTimer > 0.42) 
		{
			throwingSprite->reset();
			system->projectileManager->Create(2, rect->left + rect->width, rect->top,0);
			state = 2; 
			internalTimer = 0;
		}
		break;
	case 2: //windDown
		windDownSprite->update(deltatime);
		sprite = windDownSprite->getSprite();
		internalTimer += deltatime;
		if (internalTimer > 0.24)
		{
			windDownSprite->reset();
			state = 0;
			sprite = idleSprite;
			internalTimer = 0;
		}
	}
}

sf::FloatRect * Human::getRect()
{
	return rect;
}

sf::Sprite * Human::getSprite()
{
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

void Human::hit(float power)
{
}

void Human::hitLightning() {}
void Human::hitBranch() {}

bool Human::collides()
{
	return colliding;
}

void Human::flee()
{
}

void Human::activate()
{
	state = 0;
	colliding = false;
}

void Human::setPlayer(Player* playerIn) { player = playerIn; }

int Human::getType() { return 4; }