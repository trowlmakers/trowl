#include "ThunderCloud.h"
#include <iostream>

#include <SFML\Audio.hpp>
#include <SFML\Graphics.hpp>
#include "AnimatedSprite.h"

ThunderCloud::ThunderCloud() { std::cout << "ThunderCloud created using wrong constructor" << std::endl; }
ThunderCloud::~ThunderCloud() {	}

ThunderCloud::ThunderCloud(float xPos, float yPos)
{
	idleTexture.loadFromFile("../assets/SimpleImages/Thundercloud.png");
	idleSprite = new sf::Sprite();
	idleSprite->setTexture(idleTexture);
	idleSprite->setPosition(0, 0);

	retSprite = idleSprite;

	windUpSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Thunder_windup_sprsheet.png",
		"../assets/AnimatedObjects/Thunder_windup_sprsheet.txt");
	windDownSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Thunder_winddown_sprsheet.png",
		"../assets/AnimatedObjects/Thunder_winddown_sprsheet.txt");
	lightningSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Lightningstrike_sprsheet.png",
		"../assets/AnimatedObjects/Lightningstrike_sprsheet.txt");
	thunderSound.loadFromFile("../assets/AUDIO/ThunderStrike.wav");

	pos.x = xPos;
	pos.y = yPos;

	rect = new sf::FloatRect(pos.x, 0, 0, 0);	   

	state = 0;	  
	internalTimer = 4.0f;	 
}

void ThunderCloud::Update(float deltaTime) 
{
	pos.x -= deltaTime * 500;
	internalTimer -= deltaTime;		

	switch (state)
	{
	case 0:		  //idle
		if (internalTimer <= 0)
		{
			rect->left = pos.x;
			internalTimer = 0.36f;
			state = 1;
			std::cout << "enter windup" << std::endl;
		}
		break;
	case 1:	 //windUp				
		windUpSprite->update(deltaTime);
		retSprite = windUpSprite->getSprite();
		if (internalTimer <= 0)
		{
			internalTimer = 0.5f;
			state = 2;
			std::cout << "enter lightning" << std::endl;
			windUpSprite->reset();
			rect->width = 50;
			rect->height = 465;
			rect->left = pos.x + 285;
			rect->top = 20;
			soundOut.setBuffer(thunderSound);
			soundOut.play();
			//PLAY THUNDER AUDIO
		}
		break;
	case 2:	 //lightning
		lightningSprite->update(deltaTime);
		retSprite = lightningSprite->getSprite();
		rect->left = pos.x + 285;
		if (internalTimer <= 0)
		{
			internalTimer = 0.36f;
			state = 3;
			lightningSprite->reset();
			rect->width = 0; rect->height = 0; rect->top = 0; rect->left = pos.x;
		}
		break;
	case 3:	//windDown
		windDownSprite->update(deltaTime);
		retSprite = windDownSprite->getSprite();
		if (internalTimer <= 0)
		{
			internalTimer = 4;
			state = 0;
			retSprite = idleSprite;
			windDownSprite->reset();
		}
		break;
	}
}  

void ThunderCloud::Activate(float xPos, float yPos)
{
	state = 0;
	internalTimer = 4.0f;
	pos.x = xPos;
	rect->left -= xPos;
	rect->top = yPos;
}

sf::Sprite* ThunderCloud::GetSprite() {
	retSprite->setPosition(pos.x,pos.y);
	
	return retSprite;
}

sf::FloatRect* ThunderCloud::GetRect() 
{
	if (state != 2) { rect->left = pos.x; }
	return rect;
}
	 
int ThunderCloud::GetType()
{
	return 3;
}

void ThunderCloud::SetAudioOffset(float value)
{
}
