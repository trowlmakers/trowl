#pragma once

#include "Falcon.h"
#include "Player.h"

#include "AnimatedSprite.h"

#include <SFML\Audio.hpp>
#include <iostream>

Falcon::Falcon() {}
Falcon::~Falcon() {}

Falcon::Falcon(float xPos, float yPos) {
	speed = 300;

	idleSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/falcon_flap_sprite_sheet.png",
		"../assets/AnimatedObjects/falcon_flap_sprite_sheet.txt");
	divingSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/falcon_dive_sprite_sheet.png",
		"../assets/AnimatedObjects/falcon_dive_sprite_sheet.txt"
		);			
	hitSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Falcon_hit_sprite_sheet.png",
		"../assets/AnimatedObjects/Falcon_hit_sprite_sheet.txt"
		);
	elecSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/falcon_elec_sprite_sheet.png",
		"../assets/AnimatedObjects/falcon_elec_sprite_sheet.txt");

	hitSound.loadFromFile("../assets/AUDIO/EnemyFalconHit.wav");
	fleeSound.loadFromFile("../assets/AUDIO/EnemyRetreat.wav");
	diveSound.loadFromFile("../assets/AUDIO/FalconDiveEnemy.wav");
	elecSound.loadFromFile("../assets/AUDIO/Electrocution.wav");

	sprite = idleSprite->getSprite();

	rect = new sf::FloatRect(
		xPos,
		yPos + 90,
		sprite->getLocalBounds().width,
		35);

	pos.x = xPos;
	pos.y = yPos;

	state = 0;
	internalTimer = 0;
	colliding = true;
	hitPoints = 1;
}


void Falcon::Activate(float xPos,float yPos)
{
	pos.x = xPos;
	pos.y = yPos;

	sprite = idleSprite->getSprite();
	rect->top = pos.y + 90;
	rect->width = sprite->getLocalBounds().width;
	rect->height = 35;	

	state = 0;
	colliding = true;
	speed = 300;
	hitPoints = 1;
}

void Falcon::Update(float deltatime) {
	switch (state)
	{
	case 0: //normal
		pos.x -= speed*deltatime;	

		{
			idleSprite->update(deltatime);
			sprite = idleSprite->getSprite();

			float tempPoint =   //one float representing a diagonal line recieved from adding xpos and ypos
				rect->left + rect->width +
				rect->top + rect->height - 125;	

			if (tempPoint > player->getRect()->left + player->getRect()->top &&
				tempPoint <	player->getRect()->left + player->getRect()->width +
				player->getRect()->top + player->getRect()->height &&
				rect->top < player->getRect()->top)
			{
				soundOut.setBuffer(diveSound);
				soundOut.play();

				speed *= 3;
				state = 3;
			}
		}
		break;
	case 1:				  //fleeing

		idleSprite->update(deltatime);
		sprite = idleSprite->getSprite();

		pos.x -= speed* deltatime;
		pos.y -= speed* deltatime;
		break;
	case 2:						   //hitstun
		hitSprite->update(deltatime);
		sprite = hitSprite->getSprite();

		internalTimer -= deltatime;
		pos.x += internalTimer * 20;

		if (internalTimer < 0)
		{
			if (hitPoints > 0)
			{
				state = 0;
			}
			else
			{
				soundOut.setBuffer(fleeSound);
				soundOut.play();
				state = 1;
				colliding = false;
			}
		}
		break;
	case 3: // angle diving
		divingSprite->update(deltatime);
		sprite = divingSprite->getSprite();
		rect->height = sprite->getLocalBounds().height;
		rect->width = sprite->getLocalBounds().width;
		rect->top = pos.y;
		
		pos.x -= speed* deltatime;
		pos.y += speed* deltatime;	
		break;
	case 4:
		internalTimer -= deltatime;
		if (internalTimer <= 0)
		{
			pos.x = -50;
			state = 1;
		}  
		elecSprite->update(deltatime);
		sprite = elecSprite->getSprite();
		break;
	}
}

sf::FloatRect * Falcon::getRect()
{
	rect->left = pos.x;
	return rect;
}

sf::Sprite * Falcon::getSprite()
{
	sprite->setPosition(pos.x, pos.y);
	return sprite;
}

void Falcon::hit(float power)
{
	soundOut.setBuffer(hitSound);
	soundOut.play();
	hitPoints -= power;
	state = 2;
	internalTimer = power / 2;
}

void Falcon::hitLightning()
{
	if (state != 4)
	{
		internalTimer = 0.78;
	}

	state = 4;
}
void Falcon::hitBranch() 
{
	soundOut.setBuffer(hitSound);
	soundOut.play();
	hitPoints -= 3;
	state = 2;
	internalTimer = 0.5;
}

bool Falcon::collides()
{
	return colliding;
}	

void Falcon::setPlayer(Player* playerIn) { player = playerIn; }

int Falcon::getType() { return 2; }

void Falcon::flee()
{
	state = 1;
	colliding = false;
}
