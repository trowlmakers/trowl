#pragma once
#include <SFML\Graphics.hpp>

class Player;
struct System;

class Camera {
public:
	Camera(System* inputSystem, float xPos, float yPos);
	Camera(System* inputSystem, Player* playerObj);
	~Camera();

	void Update();

	void SetCenter(float posX, float posY);

	void SetOffset(float i);

private:
	sf::View camera;
	System* system;
	Player* player;

	float posX, posY, minX;
};