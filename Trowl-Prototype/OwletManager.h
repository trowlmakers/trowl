#pragma once

#include <SFML\Graphics.hpp>
#include "baseState.h"

struct System;
class Player;
class Owlet;

class OwletManager{
public:
	OwletManager();
	~OwletManager();

	void Initialize(System*);
	void Create(float);
	void Update(float);
	bool CollidesWith(sf::FloatRect, int);
	void Draw();
	void DrawHitBoxes();
	void PutInPool();
	void ObstacleIn(sf::FloatRect, int);
	int GetOwlets();

	Owlet * GetFollowObj(int i);

private:
	System* system;

	sf::RectangleShape hitbox;

	std::vector<Owlet*> inactiveOwlet;
	std::vector<Owlet*> activeOwlet;
	std::vector<Owlet*> takenOwlet;

	int owletsTaken = 0;
};