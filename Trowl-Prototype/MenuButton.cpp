#pragma once

#include "MenuButton.h"
#include "MenuState.h"
#include "baseState.h"

#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <string>
#include <iostream>

MenuButton::MenuButton() {}
MenuButton::~MenuButton() {}

MenuButton::MenuButton(std::string inactivePictureFileName, std::string activePictureFileName)
{
	//system = inputSystem;
	inactiveTexture.loadFromFile(inactivePictureFileName);
	activeTexture.loadFromFile(activePictureFileName);

	sprite = new sf::Sprite();
	sprite->setTexture(inactiveTexture);
	sprite->setPosition(20, 20);

	rect = new sf::IntRect(
		sprite->getLocalBounds().left,
		sprite->getLocalBounds().top,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	clickSound.loadFromFile("../assets/AUDIO/ButtonClick.wav");	
}

void MenuButton::setButtonPosition(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
	sprite->setPosition(sf::Vector2f(xPos, yPos));

}

void MenuButton::setWindow(System* inputSystem)
{
	system = inputSystem;
}

bool MenuButton::update()
{

//	std::cout << mouseXPos << "." << mouseYPos << std::endl;
	sf::Vector2f vect;
	vect = system->window.mapPixelToCoords(sf::Vector2i(
		sf::Mouse::getPosition(system->window).x,
		sf::Mouse::getPosition(system->window).y), 
		system->window.getView());

	if (rect->contains(static_cast <sf::Vector2i>(vect))) {

		sprite->setTexture(activeTexture);		   //if mouse is above button, highlight it.
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) 
		{
			soundOut.setBuffer(clickSound);
			soundOut.play();
			return true;
		}		  //if mouse is pressed, button is hit.
	}
	else { 
		sprite->setTexture(inactiveTexture);	  //if mouse is not above button, default sprite.
	}
	return false;
}

sf::Sprite * MenuButton::getSprite()
{
	return sprite;
}

sf::IntRect * MenuButton::getRect()
{
	return rect;
}

void MenuButton::setType(int typeIn)
{
	type = typeIn;
}

int MenuButton::getType()
{
	return type;
}

void MenuButton::SetSprite(std::string source) {
	inactiveTexture.loadFromFile(source);
	activeTexture.loadFromFile(source);

	sprite->setTexture(inactiveTexture);
}