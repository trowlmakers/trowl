#pragma once

#include <SFML\Graphics.hpp>
#include "baseState.h"
struct system;

class MenuButton
{
public:
	MenuButton();
	~MenuButton();
	MenuButton(
		std::string inactivePictureFileName,
		std::string activePictureFileName);

	void setButtonPosition(float xPos, float yPos);

	void setWindow(System* inputSystem);

	bool update();

	sf::Sprite* getSprite();
	sf::IntRect* getRect();

	void setType(int typeIn);

	int getType();

	void SetSprite(std::string source);

private:
	int type;

	sf::Sound soundOut;
	sf::SoundBuffer clickSound;

	System* system;
	sf::IntRect* rect;
	sf::Sprite* sprite;
	sf::Texture activeTexture;
	sf::Texture inactiveTexture;		  
};

