#include "AnimatedSprite.h"
#include <iostream>
#include <fstream>

#include <SFML\Graphics.hpp>


AnimatedSprite::AnimatedSprite()
{
	std::cout << "animated sprite initiated using wrong constructor" << std::endl;
}

// Have txtfile point to imagefile
AnimatedSprite::AnimatedSprite(std::string imageFileName, std::string txtFileName)
{
	privateTexture = new sf::Texture();
	privateTexture->loadFromFile(imageFileName);
			
	sprite = new sf::Sprite();
	sprite->setTexture(*privateTexture);


	xBounds = sprite->getLocalBounds().width;
	currentXPos = 0;

	std::ifstream infile(txtFileName);

	infile >> frameXSize;
	frameYSize = sprite->getLocalBounds().height;
	infile >> timeBetweenSwap;
	infile.close();

	sprite->setTextureRect(sf::IntRect(0, 0, frameXSize, frameYSize));
}


AnimatedSprite::~AnimatedSprite()
{
}

//have it return sf::Sprite* so that we do not need to make two calls
//OR have it return if it has looped or not, so that it can be looped only once
void AnimatedSprite::update(float deltaTime) {

	timeAcumulator += deltaTime;
	while (timeAcumulator > timeBetweenSwap) {
		timeAcumulator -= timeBetweenSwap;
		currentXPos += frameXSize;

		//if end of current frame is outside bounds, restart from first frame
		if (currentXPos + frameXSize > xBounds) { currentXPos = 0; }

		sprite->setTextureRect(sf::IntRect(currentXPos, 0, frameXSize, frameYSize));
	}			   
}


sf::Sprite* AnimatedSprite::getSprite()
{
	return sprite;
}

void AnimatedSprite::setAnimationPosition(float xPos, float yPos) 
{
	sprite->setPosition(xPos, yPos); 
}

void AnimatedSprite::reset() 
{
	currentXPos = 0; 
	timeAcumulator = 0;
}

