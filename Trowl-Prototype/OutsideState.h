#pragma once

#include <SFML\Graphics.hpp>
#include "baseState.h"

class PlayerProjectile;
class Player;
class Sprite;
class Texture;
class AlphaEnemy;
class StateSwitch;
class AlphaEnemy;
class AnimatedThing;
class Hud;
class Human;

class OutsideState : public baseState
{
public:
	OutsideState();
	~OutsideState();
	bool Update(float deltaTime);
	void Initiate(System* system);
	void Enter();
	void Exit();
	int NextState();
	int GetStateType();
private:
	sf::Music bgmMusic;
	sf::Music trainAmbient;
	System* system;
	Hud* hud;

	sf::Vector2i map[100][100];
	sf::Sprite tiles;
	sf::Texture tileTexture;
};
				 
