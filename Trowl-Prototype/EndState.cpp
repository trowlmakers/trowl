#pragma once

#include "EndState.h"
#include "baseState.h"

#include "Player.h"
#include "Camera.h"
#include "MenuButton.h"

#include <iostream>
#include <fstream>

#include <sstream>
#include <string>

#include <SFML\Main.hpp>
#include <SFML\Window.hpp>
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>

Camera* endCamera;
MenuButton* returnButton;
sf::Music endMusic;

EndState::EndState(){}
EndState::~EndState() {}

void EndState::Initiate(System* inputSystem) { 
	system = inputSystem;

	endCamera = new Camera(system, 0, 0);

	textFont = new sf::Font();
	if (!textFont->loadFromFile("../assets/font.ttf")) { std::cout << "error loading font" << std::endl; }


	scoreTex.loadFromFile("../assets/menu-images/Score_button.png");
	score = new sf::Sprite;
	score->setTexture(scoreTex);
	score->setOrigin(score->getLocalBounds().width / 2, 0);
	score->setPosition(system->screenWidth / 2, 140);

	returnButton = new MenuButton(
		"../assets/menu-images/Back_button.png",
		"../assets/menu-images/Back_button_hoover.png");
	returnButton->setButtonPosition(
		system->screenWidth / 2 - returnButton->getRect()->width / 2,
		system->screenHeight - 165);
	returnButton->setType(0);
	returnButton->setWindow(system);

}

bool EndState::Update(float deltaTime) { 
	system->window.clear(sf::Color(80, 30, 30, 1));
	system->window.draw(*bg);
	system->window.draw(*score);
	system->window.draw(*returnButton->getSprite());

	endCamera->Update();

	if (returnButton->update()) {
		return 0;
	}

	sf::Text tempText = sf::Text();
	tempText.setFont(*textFont);
	tempText.setCharacterSize(30);
	tempText.setColor(sf::Color::White);
	tempText.setPosition(sf::Vector2f(system->screenWidth / 2, 200));

	sf::Text owlets = sf::Text();
	owlets.setFont(*textFont);
	owlets.setCharacterSize(30);
	owlets.setColor(sf::Color::White);
	owlets.setPosition(sf::Vector2f(system->screenWidth / 2 - 250, 275));

	std::string Result;          // string which will contain the result  
	std::ostringstream convert;   // stream used for the conversion		  
	convert << system->score;      // insert the textual representation of 'Number' in the characters in the stream
	Result = convert.str(); // set 'Result' to the contents of the stream
	tempText.setString(Result);

	std::string tempOwl;
	tempOwl.append("Owlets Collected: ");
	std::string vadSomHelst;
	std::ostringstream convertOwl;
	convertOwl << system->owletManager->GetOwlets();
	vadSomHelst = convertOwl.str();
	tempOwl.append(vadSomHelst);
	tempOwl.append("/4");
	owlets.setString(tempOwl);
	
	system->window.draw(owlets);
	system->window.draw(tempText);

	system->window.display();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::O)) { return false; }

	return true;
}

void EndState::Enter()
{												 					 
	//system->freezeScreen->Freeze("../assets/SimpleImages/Game_over_screen.png");  

	if (system->hasWon) {
		bgTex.loadFromFile("../assets/SimpleImages/Win_screen.png");
		endMusic.openFromFile("../assets/AUDIO/WinningMusic.wav");
	}
	else {
		bgTex.loadFromFile("../assets/SimpleImages/Game_over_screen.png");
		endMusic.openFromFile("../assets/AUDIO/Game_over_screen_music.wav");
	}
	bg = new sf::Sprite;
	bg->setTexture(bgTex);

	endMusic.setLoop(true);
	endMusic.setVolume(100);
	endMusic.play();

	system->score = system->score * ((system->owletManager->GetOwlets() + 2) / 1.5);

	std::ifstream infile("../assets/HighScore.txt");
	int score1; infile >> score1;
	int score2; infile >> score2;
	int score3;	infile >> score3;
	int score4;	infile >> score4;
	int score5;	infile >> score5;
	infile.close();										

	if (system->score < score5) {}
	else if (system->score > score5 && system->score < score4)
	{
		score5 = system->score;
	} 
	else if (system->score > score4 && system->score < score3)
	{
		score5 = score4;
		score4 = system->score;
	}
	else if (system->score > score3 && system->score < score2) 
	{
		score5 = score4;
		score4 = score3;
		score3 = system->score;
	}
	else if (system->score > score2 && system->score < score1) 
	{
		score5 = score4;
		score4 = score3;
		score3 = score2;
		score2 = system->score;
	}
	else if (system->score > score1)
	{
		score5 = score4;
		score4 = score3;
		score3 = score2;
		score2 = score1;
		score1 = system->score;
	}
	
	std::ofstream file;
	file.open("../assets/HighScore.txt");
	file << score1; file << std::endl;
	file << score2;	file << std::endl;
	file << score3;	file << std::endl;
	file << score4;	file << std::endl;
	file << score5;	file << std::endl;
	file.close();	

	std::ofstream outfile;
//		outfile.open("../assets/HighScore.txt", ios::trunc);	
}

void EndState::Exit() {
	endMusic.stop();
}

int EndState::NextState()
{
	return 3;
}			
int EndState::GetStateType() { return 2; }
Player* EndState::getPlayer() { return nullptr; }
void EndState::setPlayer(Player* inputPlayer) {}

