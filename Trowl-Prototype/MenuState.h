#pragma once

class MenuButton;

#include "baseState.h"	   
#include <SFML\Graphics.hpp>

class MenuState : public baseState
{
public:
	MenuState();
	~MenuState();
	void Initiate(System* system);
	bool Update(float deltaTime);
	void Enter();
	void Exit();
	int NextState();
	int GetStateType();
	Player* getPlayer();
	void setPlayer(Player* inputPlayer);
private:
	int state;

	System* system;
	sf::Sprite* skySprite;
	sf::Texture* skyTexture;

	sf::Texture bigBoardTexture;
	sf::Sprite bigBoard;		   					 
	sf::Texture smallBoardTexture;  
	sf::Sprite smallBoard;		
	sf::Texture scoreTextTexture;
	sf::Sprite scoreText;
	sf::Texture optionsTextTexture;
	sf::Sprite optionsText;

	sf::Texture inactiveSoundButton;
	sf::Texture activeSoundButton;
	std::vector<sf::Sprite> soundButtons;

	std::vector<MenuButton*> buttons;
	bool hasBackground;
	int volumeLevel;

	std::vector<int> scores;

	sf::Music music;
};	   