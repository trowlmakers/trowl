#pragma once					
#include "SlowTime.h"

#include "AnimatedSprite.h"

#include <iostream>
#include "SFML\Graphics.hpp"

SlowTime::SlowTime()
{
	std::cout << "SlowTime Created Using Wrong Constructor" << std::endl;
}


//SlowTime::~SlowTime() {}

SlowTime::SlowTime(float xPos, float yPos) 
{
	sprite = new AnimatedSprite(
		"../assets/AnimatedObjects/PowerUpPocketWatchAnimationSpriteSheet.png",
		"../assets/AnimatedObjects/PowerUpPocketWatchAnimationSpriteSheet.txt");
		sprite->setAnimationPosition(xPos, yPos);

	rect = new sf::FloatRect(
		xPos,
		yPos,
		sprite->getSprite()->getLocalBounds().width,
		sprite->getSprite()->getLocalBounds().height);
}


void SlowTime::Update(float deltaTime)
{
	sprite->update(deltaTime);
}

sf::FloatRect * SlowTime::getRect()
{
	return rect;
}

sf::Sprite* SlowTime::getSprite()
{
	return sprite->getSprite();
}

int SlowTime::getType()
{
	return 2;
}


