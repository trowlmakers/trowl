#include "Leaf.h"

#include "baseParticle.h"

#include <SFML\Graphics.hpp>
#include <SFML\System.hpp  >
#include <iostream>

Leaf::~Leaf() {}
Leaf::Leaf()
{
	int random = rand() % 5 + 1;
	switch (random) {
	case 1:
		privateTexture.loadFromFile("../assets/SimpleImages/Leaf1.png");
		break;
	case 2:
		privateTexture.loadFromFile("../assets/SimpleImages/Leaf2.png");
		break;
	case 3:
		privateTexture.loadFromFile("../assets/SimpleImages/Leaf3.png");
		break;
	case 4:
		privateTexture.loadFromFile("../assets/SimpleImages/Leaf4.png");
		break;
	case 5:
		privateTexture.loadFromFile("../assets/SimpleImages/Leaf5.png");
		break;
	}
	sprite = new sf::Sprite();
	sprite->setTexture(privateTexture);
	sprite->setPosition(0, 0);
	int tempSpread = rand() % 200 - 25;
	spreadYDir = tempSpread / 10;

	rect = new sf::FloatRect(0,0,
		sprite->getLocalBounds().width,
		sprite->getLocalBounds().height);

	//std::cout << "leaf spawned" << std::endl;
}

void Leaf::SetPosition(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
}

void Leaf::Update(float deltatime)
{
	rect->left -= 500 * deltatime;
	rect->top += 5 * deltatime * spreadYDir;
	sprite->rotate(50 * deltatime);	
}

sf::FloatRect* Leaf::GetRect()
{
	return rect;
}

sf::Sprite* Leaf::GetSprite()
{
	sprite->setPosition(rect->left, rect->top);
	return sprite;
}

int Leaf::GetType()
{
	return 1;
}