#pragma once
#include <string>

#include <SFML\Graphics.hpp>

class AnimatedSprite
{
public:
	AnimatedSprite();
	AnimatedSprite(std::string imageFileName, std::string txtFileName);
	~AnimatedSprite();
	void update(float deltaTime);

	sf::Sprite* getSprite();

	void setAnimationPosition(float xPos, float yPos);

	void reset();

private:								  
	int frameXSize;
	int frameYSize;
	int currentXPos;
	int xBounds;
	float timeBetweenSwap;
	float timeAcumulator;
	sf::Sprite* sprite; 
	sf::Texture* privateTexture;		  
};

