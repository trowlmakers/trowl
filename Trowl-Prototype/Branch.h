#pragma once
#include "baseObstacle.h"
#include <SFML\Graphics.hpp>  

struct System;


class Branch : public baseObstacle
{
public:
	Branch();
	Branch(float xPos, float yPos);
	void Activate(float xPos, float yPos);
	~Branch();

	sf::Sprite* GetSprite();
	sf::FloatRect* GetRect();

	void Update(float deltaTime);

	int GetType();

	void SetAudioOffset(float value);

private:
	sf::Texture treeTex;
	sf::Texture branchTex;
	sf::Sprite* treeSpr;
	sf::Sprite* branchSpr;
	sf::FloatRect* rect;
};

