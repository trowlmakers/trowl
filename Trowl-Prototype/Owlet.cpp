#include "Owlet.h"
#include "SpriteCreator.h"
#include <SFML\Graphics.hpp>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <cmath>
#include "Player.h"
#include "baseState.h"
#include "AnimatedSprite.h"

#include <SFML\Audio.hpp>

Owlet::Owlet(std::string source, float posX, float posY, System* inputSystem) {
	isIdle = true;
	sprite = new sf::Sprite;
	sprCre = new SpriteCreator;
	rect = new FloatRect(posX, posY, sprite->getLocalBounds().width, sprite->getLocalBounds().height);
	system = inputSystem;

	xPos = posX;
	yPos = posY;

	sprCre->CreateSprite(*sprite, texture, source, posX, posY, 1, 1, 0, 0, 0);
	sprCre->SetOrigin(*sprite, sprite->getLocalBounds().width / 2, sprite->getLocalBounds().height);

	currMove = 0.5;
	currDir = 1;
	switchDir = 0;
	switchLean = 0;
	currLean = 1;

	hitSound.loadFromFile("../assets/AUDIO/ChickHitSound.wav");
	pickupSound.loadFromFile("../assets/AUDIO/ChickPickUp.wav");

	flyingSprite = new AnimatedSprite(
		"../assets/AnimatedObjects/Owlet_sprsheet.png",
		"../assets/AnimatedObjects/owlet_sprsheet.txt"
		);

}

Owlet::~Owlet() {

}

void Owlet::Update(float deltaTime) {
	if (isIdle) {
		if (switchDir > 150) {
			currDir *= -1;
			switchDir = 0;

			sprCre->SetScale(*sprite, currDir, 1);
			sprCre->SetRotation(*sprite, currDir * 5);
		}
		if (switchLean > 20) {
			currLean *= -1;
			switchLean = 0;
		}

		switchDir++;
		switchLean++;

		sprCre->SetPos(*sprite, sprCre->GetPos(true) + currMove * currDir, sprCre->GetPos(false));
		sprCre->SetRotation(*sprite, sprCre->GetRotation() + currLean * currMove);
	}
	else {
		int i = 1;


		float yCorrect;
		float yDif;
		float yCurrent = sprite->getPosition().y;

		if(inLine == 0)
			yCorrect = system->player->getRect()->top + (system->player->getRect()->height / 2) + (sprite->getLocalBounds().height / 2);
		else {
			yCorrect = system->owletManager->GetFollowObj(inLine)->getRect()->top;
		}
		yDif = yCorrect - yCurrent;

		if (yCurrent < yCorrect)
			yCurrent += 1 + (abs(yDif) / 25);
		else if (yCurrent > yCorrect)
			yCurrent -= 1 + (abs(yDif) / 25);

		flyingSprite->update(deltaTime);
		sprite = flyingSprite->getSprite();
		sprite->setScale(1.2, 1.2);
		sprCre->SetOrigin(*sprite, 30, 60);
		sprCre->SetPos(*sprite, system->player->getRect()->left - (65 * (inLine + 1)) * i, yCurrent);
	}

	rect = new FloatRect(sprite->getPosition().x - sprite->getLocalBounds().width/2, sprite->getPosition().y, sprite->getLocalBounds().width, -sprite->getLocalBounds().height);
}

void Owlet::ChangeState(int i) {
	if (isIdle) {
		isIdle = false;
		inLine = i;
		  
		soundOut.setBuffer(pickupSound);
		soundOut.play();

		sprCre->ChangeSprite(*sprite, texture, "../assets/OwletFlying.png");

		sf::IntRect newRect(0, 0, texture.getSize().x, texture.getSize().y);
		sprite->setTextureRect(newRect);
		sprite->setRotation(0);

		currMove = 0.1;
		switchDir = 0;
	}
	else
		isIdle = true;
}

sf::Sprite* Owlet::getSprite() {
	return sprite;
}

sf::FloatRect* Owlet::getRect() { return rect; }

bool Owlet::IsIdle() 
{
	return isIdle;
}

void Owlet::Activate(float xPos, float yPos) {
	
}