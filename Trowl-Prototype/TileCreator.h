#pragma once
#include <SFML\Graphics.hpp>

struct System;


class TileCreator {
public:
	TileCreator(float inputPosX, float inputPosY, int inputTileSizeX, int inputTileSizeY, std::string inputSource);
	~TileCreator();

	void Draw(System* inputSystem);

	void ChangePos(float inputPosX, float inputPosY);

	float GetX();

	float GetY();

	void SetColor(sf::Color inputColor);

private:
	sf::Vector2i map[150][150];
	sf::Sprite tiles;
	sf::Texture tileTexture;
	sf::Vector2i loadCounter = sf::Vector2i(0, 0);
	sf::Color color;

	float posX, posY;
	int tileX, tileY;
};