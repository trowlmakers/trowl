#pragma once

#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
class basePowerUp;

struct System;

class PowerupManager 
{
public:
PowerupManager();
~PowerupManager();
void Initialize(System* inSystem);
void Create(int selector, float xPos, float yPos);
void Update(float deltatime);
void Draw();

int CheckPowerUp();

void SetPowerUp(int selector);

private:
	System* system;

	float countDownTimer;
	int activePowerUp;

	std::vector<basePowerUp*> activepowerups;
	std::vector<basePowerUp*> inactivepowerups;	

	sf::Sound soundOut;
	sf::SoundBuffer goldenPickupSound;
	sf::SoundBuffer megaPickupSound;
	sf::SoundBuffer slowPickupSound;		
};