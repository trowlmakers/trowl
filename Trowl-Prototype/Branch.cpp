#include "Branch.h"
#include <iostream>

#include <SFML\Graphics.hpp>

Branch::Branch() { std::cout << "Branch created using wrong constructor" << std::endl; }
Branch::~Branch() {	}

Branch::Branch(float xPos, float yPos) {
	branchTex.loadFromFile("../assets/SimpleImages/Danger_branch.png");
	branchSpr = new sf::Sprite();
	branchSpr->setTexture(branchTex);
	branchSpr->setPosition(0, 0);

	rect = new sf::FloatRect(
		xPos,
		yPos,
		branchSpr->getLocalBounds().width,
		branchSpr->getLocalBounds().height);
}

void Branch::Activate(float xPos, float yPos)
{
	rect->left = xPos;
	rect->top = yPos;
}

sf::Sprite* Branch::GetSprite() {
	branchSpr->setPosition(rect->left, rect->top);
	return branchSpr;
}

sf::FloatRect* Branch::GetRect() { return rect; }

void Branch::Update(float deltaTime) { rect->left -= deltaTime * 500; }

int Branch::GetType()
{
	return 2;
}

void Branch::SetAudioOffset(float value)
{
}
