#pragma once
#include "baseObstacle.h"
#include <SFML\Graphics.hpp>  

class AnimatedSprite;


class TunnelWarning : public baseObstacle
{
public:
	TunnelWarning();
	TunnelWarning(float xPos, float yPos);
	~TunnelWarning();

	void Activate(float xPos, float yPos);

	sf::Sprite* GetSprite();
	sf::FloatRect* GetRect();

	void Update(float deltaTime);

	int GetType();

	void SetAudioOffset(float value);

	void Draw(System * system);


private:
	//sf::Texture privateTexture;
	AnimatedSprite* sprite;
	sf::FloatRect* rect;
	sf::Sprite* retSprite;
	float speed;

	sf::Music warning;
};

